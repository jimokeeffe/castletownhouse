package com.mobanode.prefs;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class AppPrefs {

	private static final String PREFS_NAME = "AppPrefs";
	private SharedPreferences appPrefs;
	private Editor editor;
	
	/**
	 * Application Preferences helper class
	 * @param ctx - Application Context
	 */
	public AppPrefs(Context ctx) {
		this.appPrefs = ctx.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE);
		this.editor = this.appPrefs.edit();
	}
	
	/**
	 * Set the last day of the year the data was updated
	 * @param day - Integer value day of the year (Calendar) 
	 */
	public void setLastUpdateDay(int day) {
		editor.putInt("updateDay", day);
		editor.commit();
	}
	
	/**
	 * Get last day content was updated
	 * @return -1 if not set before / Integer value day of the year
	 */
	public int getLastUpdateDay() {
		return appPrefs.getInt("updateDay", -1);
	}
	
	/**
	 * Mark object as favourite
	 * @param id - Title of object to favourite
	 * @param isFavourite - boolean value for favourite (true - favourite / false - not favourite)
	 */
	public void setFavourite(String id, boolean isFavourite) {
		editor.putBoolean(id, isFavourite);
		editor.commit();
	}
	
	/**
	 * Check if object is favourite or not
	 * @param id - ID of object to check
	 * @return true if object is favourite / false if object is not favourite
	 */
	public boolean isFavourite(String id) {
		return appPrefs.getBoolean(id, false);
	}
	
	/**
	 * Did parse finish when started. If not set to false. Used as a flag on splash screen to start parse if false
	 * @param didFinish - boolean true if finished, false if interrupted
	 */
	public void setDidFinishParse(boolean didFinish) {
		editor.putBoolean("didFinish", didFinish);
		editor.commit();
	}
	
	/**
	 * Check if content parse finished successfully
	 * @return true if successful / false if unsuccessful
	 */
	public boolean didFinishParse() {
		return appPrefs.getBoolean("didFinish", false);
	}
	
	/**
	 * Set flag when the application has been run for the first time
	 * @param isFirstRun - Boolean (true - first time application has been run / false - not the first time the application has been run)
	 */
	public void setIsFirstRun(boolean isFirstRun) {
		editor.putBoolean("isFirstRun", isFirstRun);
		editor.commit();
	}
	
	/**
	 * Check if this is the first time application is run
	 * @return true if first run / false if application has been ran before
	 */
	public boolean isFirstRun() {
		return appPrefs.getBoolean("isFirstRun", true);
	}
	
	/**
	 * Set flag to determine which language the application is shown in
	 * @param viewInIrish - true to view in Irish, false to view in English
	 */
	public void setViewInIrish(boolean viewInIrish) {
		editor.putBoolean("viewInIrish", viewInIrish);
		editor.commit();
	}
	
	/**
	 * Check if the application should be shown in Irish
	 * @return true if application is to be viewed in Irish, false if application is to be viewed in English
	 */
	public boolean viewInIrish() {
		return appPrefs.getBoolean("viewInIrish", false);
	}
	
	public void setIsMusicPlaying(boolean isMusicPlaying){
		editor.putBoolean("isMusicPlaying", isMusicPlaying);
		editor.commit();
	}
	public boolean isMusicPlaying(){
		return appPrefs.getBoolean("isMusicPlaying", false);
	}
}
