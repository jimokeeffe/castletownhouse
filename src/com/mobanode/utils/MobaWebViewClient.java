package com.mobanode.utils;

import android.content.Context;
import android.content.Intent;
import android.net.MailTo;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MobaWebViewClient extends WebViewClient {
	private Context context ;
	public MobaWebViewClient(Context context) {
		super();
		this.context = context ;
	}

	@Override
	public boolean shouldOverrideUrlLoading(WebView view, String url) {
		if(url.startsWith("tel:")) {
			context.startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(url)));
		}
		else if(url.startsWith("http")) {
			context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
		}
		else if(url.startsWith("sms")) {
			Intent smsIntent = new Intent(Intent.ACTION_SENDTO);
			smsIntent.setData(Uri.parse(url));
			context.startActivity(Intent.createChooser(smsIntent, "Send"));
		}
		else if(url.startsWith("mailto")) {
			MailTo mt = MailTo.parse(url);
			Intent emailIntent = new Intent(Intent.ACTION_SEND);
			emailIntent.setType("text/plain");
			emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{mt.getTo()});
			context.startActivity(Intent.createChooser(emailIntent, "Send"));//emailIntent);
		}
		else {
			context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
		}
		return true;
	}

}
