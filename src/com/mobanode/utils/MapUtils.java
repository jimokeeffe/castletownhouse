package com.mobanode.utils;

import java.util.List;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;

public class MapUtils {
	
	/**
	 * Zoom MapView to show all pins
	 * @param geoPoints - List of GeoPoints to zoom to
	 * @param mapView - MapView used to show pins
	 */
	public static void zoomMap(List<GeoPoint> geoPoints, MapView mapView) {
		if (null == geoPoints) return;

		int minLatitude = Integer.MAX_VALUE, maxLatitude = Integer.MIN_VALUE;
		int minLongitude = Integer.MAX_VALUE, maxLongitude = Integer.MIN_VALUE;
		for (GeoPoint geoPoint : geoPoints) {
			int latitude = geoPoint.getLatitudeE6();
			int longitude = geoPoint.getLongitudeE6();
			
			minLatitude = Math.min(latitude, minLatitude);
			maxLatitude = Math.max(latitude, maxLatitude);
			minLongitude = Math.min(longitude, minLongitude);
			maxLongitude = Math.max(longitude, maxLongitude);
		}
		
		int latitudeSpan = Math.abs(maxLatitude - minLatitude);
		int longitudeSpan = Math.abs(maxLongitude - minLongitude);
		
		mapView.getController().zoomToSpan(latitudeSpan, longitudeSpan);
		mapView.getController().animateTo(new GeoPoint(minLatitude + latitudeSpan / 2, minLongitude + longitudeSpan / 2));
	}
	
	/**
	 * Sets the distance from each Festival in a list to the users location
	 * @param location - users location
	 * @param ctx - Application context
	 */
//	public static void setFestivalsDistanceFromMe (Location location, Context ctx, ArrayList<Festival> festivals) {
//		for(Festival f : festivals) {
//			if(!f.getLatitude().equals("") && !f.getLongitude().equals(""))
//				f.setDistanceToMe(getDistance(f.getLatitude(), f.getLongitude(), location));
//			else
//				f.setDistanceToMe("999.00");
//		}
//		
//		Collections.sort(festivals, new SortByDistance());
//	}
	
	/**
	 * Get the distance between a location and a GeoPoint
	 * @param lat - Geo-point latitude
	 * @param lng - Geo-point longitude
	 * @param location - User location
	 * @return
	 */
//	private static String getDistance(String lat, String lng, Location location) {
//		double distance;
//
//		double radius = 6371.0f;
//
//		double longOne = Double.parseDouble(lng);
//		double longTwo = location.getLongitude();
//		double latOne = Double.parseDouble(lat);
//		double latTwo = location.getLatitude();
//
//		double x = latTwo - latOne;
//		double y = longTwo - longOne;
//
//		double dLat = myDegToRad(x);
//		double dLong = myDegToRad(y);
//
//		double lat1 = myDegToRad(latOne);
//		double lat2 = myDegToRad(latTwo);
//
//		double a = Math.sin((dLat / 2)) * Math.sin((dLat / 2))
//				+ Math.sin((dLong / 2)) * Math.sin((dLong / 2))
//				* Math.cos(lat1) * Math.cos(lat2);
//
//		double z = 1 - a;
//
//		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(z));
//
//		DecimalFormat decFormat = new DecimalFormat("###.##");
//		distance = radius * c;
//
//		return decFormat.format(distance);
//	}

	/**
	 * Convert Degrees to Radians
	 * @param deg
	 * @return
	 */
//	private static double myDegToRad(double deg) {
//		return ((Math.PI / 180) * deg);
//	}
}
