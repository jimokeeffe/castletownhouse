package com.mobanode.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.mobanode.prefs.AppPrefs;

public class CommonUtils {

	/**
	 * Determine whether the application information should be updated. If one day has passed, info should be updated.
	 * @param ctx - Application context
	 * @return true if info needs to be updated / false if info does not need to be updated 
	 */
	public static boolean shouldUpdateInfo(Context ctx) {
		AppPrefs prefs = new AppPrefs(ctx);
		Calendar today = Calendar.getInstance();
		
		if(today.get(Calendar.DAY_OF_YEAR) > prefs.getLastUpdateDay()) {
			prefs.setLastUpdateDay(today.get(Calendar.DAY_OF_YEAR));
			return true;
		}
		else return false;
	}
	
	
	/**
	 * Get pixel value from Padding DP
	 * @param context - Application Context
	 * @param padding - Padding DP
	 * @return
	 */
	public static int getPixelsFromPadding(Context context, int padding) {
		final float scale = context.getResources().getDisplayMetrics().density;
		int widthInPx = (int) (padding * scale + 0.5f);
		return widthInPx;
	}
	
	/**
	 *  Process Titter / Facebook post dates for readability in list
	 * @param dateStr - Date String from feed
	 * @param postType - Post type "twitter" or "facebook"
	 * @return Returns formatted date as String
	 */
	public static String processSocialPostDate(String dateStr, String postType) {
		SimpleDateFormat dateFormat;
		if(postType.equals(AppConstants.TWITTER_DATE_IDENTIFIER))
			dateFormat = new SimpleDateFormat(AppConstants.TWITTER_DATE_FORMAT, Locale.getDefault());//SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
		else // Facebook
			dateFormat = new SimpleDateFormat(AppConstants.FACEBOOK_DATE_FORMAT, Locale.getDefault());
		
		long date = 0;
		
		try {
			date = dateFormat.parse(dateStr).getTime();
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
		
		Date dateCreated = new Date(date + TimeZone.getDefault().getRawOffset() + (TimeZone.getDefault().inDaylightTime(new Date()) ? TimeZone.getDefault().getDSTSavings() : 0));
		Date now = new Date();
		
		long differenceInMins = (now.getTime() - dateCreated.getTime()) / 60000;
		
		if(differenceInMins <= 1)
			return "Less than 1 minute ago";
		else if (1 <= differenceInMins && differenceInMins <= 60)
			return differenceInMins + "  minutes ago";
		else if (60 <= differenceInMins && differenceInMins <= 89)
			return "About 1 hour ago";
		else if (90 <= differenceInMins && differenceInMins <= 1439)
			return "About " + (differenceInMins / 60) + " hours ago";
		else if ( 1440 <= differenceInMins && differenceInMins <= 2879 )
	        return "1 day ago";
	    else if ( 2880 <= differenceInMins && differenceInMins <= 43199 )
	        return (differenceInMins / 1440) + " days ago";
	    else if ( 43200 <= differenceInMins && differenceInMins <= 86399 )
	        return "About 1 month ago";
	    else if ( 86400 <= differenceInMins && differenceInMins <= 525599 )
	        return "About " + (differenceInMins / 43200) + " months ago";
	    else 
	        return "About " + (differenceInMins / 525600) + " years ago";
	}
	
	/**
	 * Checks for an Internet connection
	 * @param ctx - Application context
	 * @return returns true if connection available and false if none is available
	 */
	public static boolean isNetworkAvailable(Context ctx) {
		ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo nInfo = cm.getActiveNetworkInfo();

		if (nInfo != null && nInfo.isConnected())
			return true;
		else
			return false;
	}
	
	
//	/**
//	 * Get drawable for specific weather image
//	 * @param name - name of image
//	 * @param context - application context
//	 * @return drawable of specific image or cloud image as default
//	 */
//	public static Drawable loadWeatherImageFromAssests(Context context, String name) {
//		try {
//			InputStream in = context.getAssets().open("weather-icons/" + name + ".png");
//			Drawable d = Drawable.createFromStream(in, null);
//			return d;
//		} catch (IOException e) {
//			e.printStackTrace();
//			
//			try {
//				InputStream in = context.getAssets().open("weather-icons/cloudy.png");
//				Drawable d = Drawable.createFromStream(in, null);
//				return d;
//			} catch (IOException e1) {
//				e1.printStackTrace();
//				return null;
//			}
//		}
//	}
	
	public static Drawable loadWeatherImageFromDrawables(Context context, String name) {
		name = name.replace("-", "_");
		
		Drawable d;
		
		try {
			d = context.getResources().getDrawable(context.getResources().getIdentifier(name, "drawable", context.getPackageName()));
		} catch (Resources.NotFoundException e) {
			d = context.getResources().getDrawable(context.getResources().getIdentifier("cloudy", "drawable", context.getPackageName()));
		}
		
		return d;
	}
}
