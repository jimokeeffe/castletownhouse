package com.mobanode.utils;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.mobanode.castletownhouse.R;
import com.mobanode.prefs.AppPrefs;

public class PlayAudio extends Service {
	private static final String LOGCAT = "Play Audio";
	MediaPlayer objPlayer;
	private final IBinder mBinder = new AudioBinder();

	public void onCreate() {
		super.onCreate();
		Log.d(LOGCAT, "Service created");
		objPlayer = MediaPlayer.create(this, R.raw.castletownhouse_audio); // insert
																			// mp3
																			// file
		// here
	}

	public int onStartCommand(Intent intent, int flags, int startId) {
		// objPlayer.start();
		Log.d(LOGCAT, "service started");
		// if (objPlayer.isLooping() != true) {
		// Log.d(LOGCAT, "Problem with audio");
		// }
		return START_NOT_STICKY;
	}

	@Override
	public void onDestroy() {
		Log.d(LOGCAT, "service destroyed");
		super.onDestroy();
		objPlayer.stop();
		objPlayer.release();
	}

	@Override
	public IBinder onBind(Intent objIntent) {
		// TODO Auto-generated method stub
		return mBinder;
	}

	public class AudioBinder extends Binder {
		public PlayAudio getService() {
			return PlayAudio.this;
		}
	}

	public void play() {
		AppPrefs appPrefs = new AppPrefs(this);
		appPrefs.setIsMusicPlaying(true);

		if (objPlayer == null) {
			objPlayer = MediaPlayer.create(this, R.raw.castletownhouse_audio);
		}
		if (!objPlayer.isPlaying()) {
			objPlayer.start();
		}
	}

	public void pause() {
		AppPrefs appPrefs = new AppPrefs(this);
		appPrefs.setIsMusicPlaying(false);

		if (objPlayer == null) {
			objPlayer = MediaPlayer.create(this, R.raw.castletownhouse_audio);
		}
		if (objPlayer.isPlaying()) {
			objPlayer.pause();
		}
	}

	public boolean isPlaying() {
		if (objPlayer == null) {
			objPlayer = MediaPlayer.create(this, R.raw.castletownhouse_audio);
		}

		return objPlayer.isPlaying();

	}

	public void startAgain() {
		if (objPlayer == null) {
			objPlayer = MediaPlayer.create(this, R.raw.castletownhouse_audio);
		}
		if (objPlayer.isPlaying()) {
			objPlayer.pause();

		}
		objPlayer.seekTo(0);
		objPlayer.start();
	}
}
