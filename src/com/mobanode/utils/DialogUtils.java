package com.mobanode.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;

import com.mobanode.dublincastle.KilkennyCastleApplication;
import com.mobanode.castletownhouse.R;
import com.mobanode.prefs.AppPrefs;

public class DialogUtils {

	/**
	 * Show thank you dialog after image successfully uploaded
	 */
	public static void showThankYouDialog(final Context ctx) {
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setTitle(R.string.thank_you_title).setMessage(new AppPrefs(ctx).viewInIrish() ? R.string.thank_you_upload_label_ie : R.string.thank_you_upload_label)
			.setPositiveButton(android.R.string.ok , new OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					((Activity) ctx).finish();
				}
			}).show();
	}
	
	public static void showErrorDialog(final Context ctx) {
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setTitle(R.string.error_title).setMessage(R.string.error_message)
			.setPositiveButton(android.R.string.ok , new OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					((Activity) ctx).finish();
				}
			}).show();
	}
	
	public static void showGpsCoordsDialog(final Context ctx, String lat, String lng) {
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		
		String message;
		String title;
		
		final String smsBody = ctx.getResources().getString(R.string.latitude_text_view_label)
				+ ": " + lat + ", "
				+ ctx.getResources().getString(R.string.longitude_text_view_label)
				+ ": " + lng;
		
		if(KilkennyCastleApplication.getPrefsInstance().viewInIrish()) {
			title = ctx.getResources().getString(R.string.my_location_dialog_title_ie);
			message = ctx.getResources().getString(R.string.latitude_text_view_label)
					+ ":\n" + lat + "\n\n"
					+ ctx.getResources().getString(R.string.longitude_text_view_label)
					+ ":\n" + lng;
		}
		else {
			title = ctx.getResources().getString(R.string.my_location_dialog_title);
			message = ctx.getResources().getString(R.string.latitude_text_view_label)
						+ ":\n" + lat + "\n\n"
						+ ctx.getResources().getString(R.string.longitude_text_view_label)
						+ ":\n" + lng;
		}
		
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setPositiveButton(R.string.sms_btn_label , new OnClickListener() {
				
			public void onClick(DialogInterface dialog, int which) {
				Intent smsIntent = new Intent(Intent.ACTION_VIEW);
				smsIntent.putExtra("sms_body", smsBody);
				smsIntent.setType("vnd.android-dir/mms-sms");
				ctx.startActivity(smsIntent);
			}
		});
		builder.setNegativeButton(android.R.string.ok, new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		}).show();
	}
}
