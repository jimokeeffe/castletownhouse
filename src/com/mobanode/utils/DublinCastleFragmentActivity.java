package com.mobanode.utils;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;

public class DublinCastleFragmentActivity extends FragmentActivity {
	private PlayAudio service;
	public static final int MENU_ITEM_ID = 1234 ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onResume() {
		super.onResume();
		Intent intent = new Intent(this, PlayAudio.class);
		bindService(intent, mConnection, 0);
	}
	
	@Override
	protected void onPause() {
		unbindService(mConnection);
		super.onPause();
	}

	private ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder binder) {
			PlayAudio.AudioBinder b = (PlayAudio.AudioBinder) binder;
			service = b.getService();
		}

		public void onServiceDisconnected(ComponentName className) {
			service = null;
		}
	};

	public void playAudio() {
		if (service != null) {
			service.play();
		}
	}

	public void pauseAudio() {
		if (service != null) {
			service.pause();
		}
	}
}
