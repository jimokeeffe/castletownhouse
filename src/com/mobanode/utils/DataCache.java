package com.mobanode.utils;

import java.util.ArrayList;
import java.util.HashMap;

import android.graphics.Bitmap;

import com.mobanode.entities.FacebookPost;
import com.mobanode.entities.Tweet;
import com.mobanode.entities.YoutubePost;

public class DataCache {

	public static ArrayList<FacebookPost> facebookPosts;
	public static ArrayList<Tweet> twitterPosts;
	public static ArrayList<String> fbImgUrls;
	public static HashMap<String, Bitmap> fbImageCache;
	
	public static ArrayList<YoutubePost> youtubePosts;
	public static ArrayList<String> ytImgUrls;
	public static HashMap<String, Bitmap> ytImageCache = new HashMap<String, Bitmap>();

	// Location cache
	public static double myLocationLat = 0.0;
	public static double myLocationLng = 0.0;

	public static double selectedLat = 0.0;
	public static double selectedLng = 0.0;

	public static void clearStorage() {
		facebookPosts = null;
		twitterPosts = null;
		fbImgUrls = null;
		fbImageCache = null;
	}
}
