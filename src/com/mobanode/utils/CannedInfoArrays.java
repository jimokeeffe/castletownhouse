package com.mobanode.utils;

import com.mobanode.castletownhouse.R;

public class CannedInfoArrays {
	
	private static final int[] DEFAULT_IMAGE = {
		R.drawable.entrance_hall_3
	};
	
	private static final int[] entrance = {
		R.drawable.entrance_hall_1,
		R.drawable.entrance_hall_2,
		R.drawable.entrance_hall_3
		
	};
	
	private static final int[] staircase_hall = {
		R.drawable.staircase_hall_1,
		R.drawable.staircase_hall_2,
		R.drawable.staircase_hall_3,
		R.drawable.staircase_hall_4
	};
	private static final int[] dining_room = {
		R.drawable.dining_room_1,
		R.drawable.dining_room_2
		
	};
	
	
	private static final int[] butlers_pantry = {
		R.drawable.butlers_pantry_1,
		R.drawable.butlers_pantry_2,
		R.drawable.butlers_pantry_3
	};
	
	private static final int[] axial_corridor = {
		R.drawable.axial_corridor_1,
		R.drawable.axial_corridor_2,
		R.drawable.axial_corridor_3
	};
	
	private static final int[] brown_study = {
		R.drawable.brown_study_1,
		R.drawable.brown_study_2
	};
	
	private static final int[] red_silk_room = {
		R.drawable.red_silk_1,
		R.drawable.red_silk_2,
		R.drawable.red_silk_3
	};
	
	private static final int[] recpetion_room = {
		R.drawable.red_silk_1
	};
	
	private static final int[] print_room = {
		R.drawable.print_room_1,
		R.drawable.print_room_2,
		R.drawable.print_room_3
	};
	
	private static final int[] state_bedroom = {
		R.drawable.state_bedroom_1

	};
	
	private static final int[] healy_room = {
		R.drawable.healy_room_1
	};
	
	private static final int[] map_room = {
		R.drawable.map_room_1,
		R.drawable.map_room_2,
		R.drawable.map_room_3
	};
	
	private static final int[] blue_bedroom = {
		R.drawable.blue_bedroom_1,
		R.drawable.blue_bedroom_2,
		R.drawable.blue_bedroom_3
	};
	
	private static final int[] lady_boudoir = {
		R.drawable.boudoir_1,
		R.drawable.boudoir_2
	};
	
	private static final int[] lady_bedroom = {
		R.drawable.bedroom_1,
		R.drawable.bedroom_2,
		R.drawable.bedroom_3
	};
	
	
	private static final int[] pastel_room = {
		R.drawable.pastel_1,
		R.drawable.pastel_2,
		R.drawable.pastel_3
	};
	
	private static final int[] long_gallery = {
		R.drawable.long_gallery_1,
		R.drawable.long_gallery_2,
		R.drawable.long_gallery_3
	};
	
	private static final int[] conolly_folly = {
		R.drawable.conolly_folly_1,
		R.drawable.conolly_folly_2
	};
	
//	private static final int[] wonderful_barn = {
//		R.drawable.great_courtyard_1,
//		R.drawable.great_courtyard_2,
//		R.drawable.great_courtyard_3
//	};
//	
//	private static final int[] desmesne = {
//		R.drawable.undercroft_2,
//		R.drawable.undercroft_4
//	};
//	
	private static final int[] batty_lodge = {
		R.drawable.entrance_hall_3
	};
	
	public static int[] getTourImageList(String tag) {
		if(tag.equals("entrance"))
			return entrance;
		else if(tag.equals("staircase_hall"))
			return staircase_hall;
		else if(tag.equals("dining_room"))
			return dining_room;
		else if(tag.equals("butlers_pantry"))
			return butlers_pantry;
		else if(tag.equals("axial_corridor"))
			return axial_corridor;
		else if(tag.equals("brown_study"))
			return brown_study;
		else if(tag.equals("red_silk_room"))
			return red_silk_room;
//		else if(tag.equals("queens_bedroom"))
//			return queens_bedroom;
		else if(tag.equals("print_room"))
			return print_room;
		else if(tag.equals("state_bedroom"))
			return state_bedroom;
		else if(tag.equals("healy_room"))
			return healy_room;
		else if(tag.equals("lady_boudoir"))
			return lady_boudoir;
		else if(tag.equals("map_room"))
			return map_room;
		else if(tag.equals("blue_bedroom")) 
			return blue_bedroom;
		else if(tag.equals("lady_bedroom")) 
			return lady_bedroom;
		else if(tag.equals("pastel_room"))
			return pastel_room;
		else if(tag.equals("long_gallery")) 
			return long_gallery;
		else if(tag.equals("conolly_folly"))
			return conolly_folly;
//		else if(tag.equals("wonderful_barn")) 
//			return wonderful_barn;
//		else if(tag.equals("desmesne")) 
//			return desmesne;
		else
			return DEFAULT_IMAGE;
	}
}
