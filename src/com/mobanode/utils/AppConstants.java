package com.mobanode.utils;

public class AppConstants {
	
	
	/**
	 * Blaskets Feed URL
	 */
	 public static final String FEED_URL = "http://castletownhouseevents.wordpress.com/feed/";
	//public static final String FEED_URL = "http://blasketislandevents.wordpress.com/feed/";
	 public static final String ATRACTIONS_URL = "https://kilkennyattractions.wordpress.com/feed/";
	
	/**
	 * Blaskets WebCam URL
	 */
	public static final String WEBCAM_URL = "http://www.tg4.ie/programmes/dinglecam.html";
	
	/**
	 * Forecast URL
	 */
	public static final String FORECAST_URL = "http://mobanode.mobi/blaskets/forecast.json";
	
	/**
	 * Assets folder path
	 */
	public static final String ASSETS_HTML_FOLDER_PATH = "file:///android_asset/html/";

	// Social Constants
	public static final String TWITTER_DATE_IDENTIFIER = "twitter";
	
	public static final String FACEBOOK_DATE_IDENTIFIER = "facebook";
	
	public static final String TWITTER_DATE_FORMAT = "EEE MMM dd HH:mm:ss Z yyyy";
	
	public static final String FACEBOOK_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
	
	/**
	 * Twitter feed constants
	 */
	public static final String FEED_CONSUMER_KEY = "A7BTFdhRmoKxIOo2NWiQ";
	public static final String FEED_CONSUMER_SECRET = "oIgThwFZHZQ2gMskChY6sngCJ1QtOKYCQBb7ZUp9I";
	public static final String FEED_ACCESS_TOKEN = "35866018-bZ0jLAi40q19rR8OSl2mjAbr1Aur92HEkk4ERBsy3";
	public static final String FEED_ACCESS_TOKEN_SERCRET = "gl6spUtAjOOX5RtoARMYRjd2rH6gisdsmiHXJpnNWLE";
	
	/**
	 * Parse Application ID
	 */
	public static final String PARSE_APPLICATION_ID = "pyqGx6tB9JyCqeBAdOqN7c8gaqBtRoUsoYNyUrHW";
	
	/**
	 * Parse Client Key
	 */
	public static final String PARSE_CLIENT_KEY = "UWlfRyAvt4ZtnqzN7bRcOzAfUP1bRtBeXeCMBSvT";
	
	/**
	 * Flurry API Key
	 */
	public static final String FLURRY_API_KEY = "MXP5FSDSVKRWG9C5JGGG";
	
	/**
	 * Forecast.io API keys
	 */
	public static final String FORECAST_IO_API_KEY = "c12136fd084cb689891d4c569c2d996b";
	
	/*
	 * Carousel 
	 */
	public final static float BIG_SCALE = 1.0f;
	public final static int TOUR_PAGES = 20;
	public final static int INFO_PAGES = 4;
	
	
	/**
	 * Tour Image Tags
	 */
	public static final String[] TOUR_IMAGES_TAGS = {
			"entrance",
			"staircase_hall", 
			"dining_room",
			"butlers_pantry",
			"axial_corridor",
			"brown_study",
			"red_drawing_room",
			"recption_room",
			"print_room",
			"state_bedroom",
			"healy_room",
			"map_room",
			"blue_bedroom",
			"lady_boudoir",
			"lady_bedroom",
			"pastel_room",
			"long_gallery",
			"conolly_folly",
			"wonderful_barn",
			"desmesne",
			"batty_lodge"
			
	};
}
