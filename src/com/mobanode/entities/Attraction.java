package com.mobanode.entities;

public class Attraction {

	private String name ;
	private String content ;
	private double lat ;
	private double lon ;
	
	
	public Attraction() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Attraction(String name, String content, double lat, double lon) {
		super();
		this.name = name;
		this.content = content;
		this.lat = lat;
		this.lon = lon;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}


	public double getLat() {
		return lat;
	}


	public void setLat(double lat) {
		this.lat = lat;
	}


	public double getLon() {
		return lon;
	}


	public void setLon(double lon) {
		this.lon = lon;
	}
	
	
}
