package com.mobanode.entities;

public class MapLocation {

	private String name;
	private String nameIrish;
	private String tag;
	private double latitude;
	private double longitude;
	
	public MapLocation() {
		super();
	}

	public MapLocation(String name, String nameIrish, String tag, double latitude, double longitude) {
		super();
		this.name = name;
		this.nameIrish = nameIrish;
		this.tag = tag;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameIrish() {
		return nameIrish;
	}

	public void setNameIrish(String nameIrish) {
		this.nameIrish = nameIrish;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
}
