package com.mobanode.entities;

import org.json.JSONException;
import org.json.JSONObject;

public class WeatherForecast {

//	private long time;
	private String summary;
	private String icon;
//	private double precipIntensity;
//	private double precipProbability;
	private double temperature;
//	private double dewPoint;
//	private double windSpeed;
//	private double windBearing;
//	private double cloudCover;
//	private double humidity;
//	private double pressure;
	
	
	public WeatherForecast() {
		super();
	}


	public WeatherForecast(String summary, String icon, double temperature) {
		super();
//		this.time = time;
		this.summary = summary;
		this.icon = icon;
//		this.precipIntensity = precipIntensity;
//		this.precipProbability = precipProbability;
		this.temperature = temperature;
//		this.dewPoint = dewPoint;
//		this.windSpeed = windSpeed;
//		this.windBearing = windBearing;
//		this.cloudCover = cloudCover;
//		this.humidity = humidity;
//		this.pressure = pressure;
	}


//	public long getTime() {
//		return time;
//	}


//	public void setTime(long time) {
//		this.time = time;
//	}


	public String getSummary() {
		return summary;
	}


	public void setSummary(String summary) {
		this.summary = summary;
	}


	public String getIcon() {
		return icon;
	}


	public void setIcon(String icon) {
		this.icon = icon;
	}


//	public double getPrecipIntensity() {
//		return precipIntensity;
//	}


//	public void setPrecipIntensity(double precipIntensity) {
//		this.precipIntensity = precipIntensity;
//	}


//	public double getPrecipProbability() {
//		return precipProbability;
//	}


//	public void setPrecipProbability(double precipProbability) {
//		this.precipProbability = precipProbability;
//	}


	public double getTemperature() {
		return temperature;
	}


	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}


//	public double getDewPoint() {
//		return dewPoint;
//	}


//	public void setDewPoint(double dewPoint) {
//		this.dewPoint = dewPoint;
//	}


//	public double getWindSpeed() {
//		return windSpeed;
//	}


//	public void setWindSpeed(double windSpeed) {
//		this.windSpeed = windSpeed;
//	}


//	public double getWindBearing() {
//		return windBearing;
//	}


//	public void setWindBearing(double windBearing) {
//		this.windBearing = windBearing;
//	}


//	public double getCloudCover() {
//		return cloudCover;
//	}


//	public void setCloudCover(double cloudCover) {
//		this.cloudCover = cloudCover;
//	}


//	public double getHumidity() {
//		return humidity;
//	}


//	public void setHumidity(double humidity) {
//		this.humidity = humidity;
//	}


//	public double getPressure() {
//		return pressure;
//	}


//	public void setPressure(double pressure) {
//		this.pressure = pressure;
//	}
	
	public WeatherForecast parserJson(String json) {
		try {
			WeatherForecast forecast = new WeatherForecast();
			
			JSONObject obj = new JSONObject(json);
			
			JSONObject currently = obj.getJSONObject("currently");
			
//			forecast.setTime(currently.optLong("time"));
			forecast.setSummary(currently.optString("summary"));
			forecast.setIcon(currently.optString("icon"));
//			forecast.setPrecipIntensity(currently.optDouble("precipIntensity"));
//			forecast.setPrecipProbability(currently.optDouble("precipProbability"));
			forecast.setTemperature(currently.optDouble("temperature"));
//			forecast.setDewPoint(currently.optDouble("dewPoint"));
//			forecast.setWindSpeed(currently.optDouble("windSpeed"));
//			forecast.setWindBearing(currently.optDouble("windBearing"));
//			forecast.setCloudCover(currently.optDouble("cloudCover"));
//			forecast.setHumidity(currently.optDouble("humidity"));
//			forecast.setPrecipIntensity(currently.optDouble("pressure"));
			
			return forecast;
			
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}
}
