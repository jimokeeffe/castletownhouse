package com.mobanode.entities;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mobanode.utils.DataCache;

public class YoutubePost {

	private String title;
	private String vidUrl;
	private String thumbUrl;
	private int numViews;
	
	public YoutubePost() {
		super();
	}
	
	public YoutubePost(String title, String vidUrl, String thumbUrl,
			int numViews) {
		super();
		this.title = title;
		this.vidUrl = vidUrl;
		this.thumbUrl = thumbUrl;
		this.numViews = numViews;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getVidUrl() {
		return vidUrl;
	}

	public void setVidUrl(String vidUrl) {
		this.vidUrl = vidUrl;
	}

	public String getThumbUrl() {
		return thumbUrl;
	}

	public void setThumbUrl(String thumbUrl) {
		this.thumbUrl = thumbUrl;
	}

	public int getNumViews() {
		return numViews;
	}

	public void setNumViews(int numViews) {
		this.numViews = numViews;
	}
	
	public static void parseYoutubeJSON(String json) {
		DataCache.youtubePosts = new ArrayList<YoutubePost>();
		DataCache.ytImgUrls = new ArrayList<String>();
		
		try {
			JSONObject jsonObj = new JSONObject(json);
			JSONObject dataObj = jsonObj.getJSONObject("data");
			JSONArray itemsArray = dataObj.getJSONArray("items");
			YoutubePost ytPost;
			
			for(int i = 0; i < itemsArray.length(); i++) {
				JSONObject item = itemsArray.getJSONObject(i);
				JSONObject video = item.getJSONObject("video");
				
				if(!video.has("status")) {
					ytPost = new YoutubePost();
					
					ytPost.setTitle(video.getString("title"));
					
					if(video.has("viewCount"))
						ytPost.setNumViews(video.getInt("viewCount"));
					
					if(video.has("thumbnail")) {
						JSONObject thumbObj = video.getJSONObject("thumbnail");
						if(thumbObj.has("sqDefault")) {
							ytPost.setThumbUrl(thumbObj.getString("sqDefault"));
							DataCache.ytImgUrls.add(ytPost.getThumbUrl());
						}
					}
					
					if(video.has("player")) {
						JSONObject playerObj = video.getJSONObject("player");
						if (playerObj.has("default")) {
							ytPost.setVidUrl(playerObj.getString("default"));
						}
					}
					
					DataCache.youtubePosts.add(ytPost);					
				}
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
