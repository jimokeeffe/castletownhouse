package com.mobanode.dublincastle;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.mobanode.castletownhouse.R;
import com.mobanode.prefs.AppPrefs;
import com.mobanode.utils.DialogUtils;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.SaveCallback;

public class UploadActivity extends Activity {

//	private final String TAG = "UPLOAD-ACT";
	private static final int IMAGE_TAKEN = 0;
	private static final int IMAGE_PICKED = 1;
	
	private SaveCallback imgUpFinishedCallback;
	private SaveCallback objectUpFinishedCallback;
	private SaveCallback thumbUpFinishedCallback;
	
	private ParseFile imgFile;
	private ParseFile thumbFile;
	
	private String imgFileName;
	private File storageDir;
	
	private Bitmap thumb;
	
	private ViewFlipper vf;
	
	private ProgressDialog mProgress;
	
	private ImageView img;
	private EditText venueEditText;
	private EditText nameEditText;
	private EditText emailEditText;
	private EditText commentEditText;
	
	private AppPrefs prefs = KilkennyCastleApplication.getPrefsInstance();
	private boolean formViewShown = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_upload);
		
		vf = (ViewFlipper) findViewById(R.id.upload_view_flipper);
		img = (ImageView) findViewById(R.id.image_upload_img_view);
		venueEditText = (EditText) findViewById(R.id.venue_edit_text);
		nameEditText = (EditText) findViewById(R.id.name_edit_text);
		emailEditText = (EditText) findViewById(R.id.email_edit_text);
		commentEditText = (EditText) findViewById(R.id.comment_edit_text);
		
		// If irish set Button and ImageView text
		if(prefs.viewInIrish()) {
			setTitle(getResources().getString(R.string.upload_btn_label_ie));
			((TextView) findViewById(R.id.upload_info_text_view)).setText(getResources().getString(R.string.upload_info_text_ie));
			((Button) findViewById(R.id.galler_choice_btn)).setText(getResources().getString(R.string.gallery_btn_label_ie));
			((Button) findViewById(R.id.take_photo_btn)).setText(getResources().getString(R.string.take_photo_btn_label_ie));
			((Button) findViewById(R.id.upload_btn)).setText(getResources().getString(R.string.upload_btn_label_ie));
			
			venueEditText.setHint(getString(R.string.venue_input_hint_ie));
			nameEditText.setHint(getString(R.string.name_input_hint_ie));
			emailEditText.setHint(getString(R.string.email_input_hint_ie));
			commentEditText.setHint(getString(R.string.comment_input_hint_ie));
		}
		
		setUpParseCallbacks();
	}
	
	public void photoChoiceBtnsClick(View view) {
		switch (view.getId()) {
		case R.id.galler_choice_btn:
			Intent intent = new Intent(Intent.ACTION_PICK);
			intent.setType("image/*");
			startActivityForResult(intent, IMAGE_PICKED);
			break;
			
		case R.id.take_photo_btn:
			Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			String timeStamp = new SimpleDateFormat("yyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
			imgFileName = "IMG_" + timeStamp + ".jpg";
			storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), imgFileName);
			takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(storageDir));
			startActivityForResult(takePhotoIntent, IMAGE_TAKEN);
			break;

		default:
			break;
		}
	}
	
	public void uploadBtnClick(View view) {
		mProgress = ProgressDialog.show(UploadActivity.this, "Please wait...", "Uploading Your Image", true, true);
		uploadImage();
	}
	
	// Set up callback handlers for image, thumb and object uploads finish
	private void setUpParseCallbacks() {
		// Image has finished uploading
		imgUpFinishedCallback = new SaveCallback() {
			@Override
			public void done(ParseException arg0) {
				if(arg0 == null) {
					// Start thumb upload
					uploadThumb();
				}
				else {
					DialogUtils.showErrorDialog(UploadActivity.this);
				}
			}
		};
		
		// Thumb has finished uploading
		thumbUpFinishedCallback = new SaveCallback() {
			@Override
			public void done(ParseException arg0) {
				if(arg0 == null) {
					// Upload all data
					commitParseObject();
				}
				else {
					DialogUtils.showErrorDialog(UploadActivity.this);
				}
			}
		};
		
		// ParseObject has finished uploading
		objectUpFinishedCallback = new SaveCallback() {
			@Override
			public void done(ParseException arg0) {
				mProgress.dismiss();
				
				if(arg0 == null) {
					// Finish activity and thank user
					DialogUtils.showThankYouDialog(UploadActivity.this);
				}
				else {
					DialogUtils.showErrorDialog(UploadActivity.this);
				}
			}
		};
	}
	
	// Upload image to parse
	private void uploadImage() {
		try {
			ByteArrayOutputStream bao = new ByteArrayOutputStream();
			((BitmapDrawable) img.getDrawable()).getBitmap().compress(Bitmap.CompressFormat.JPEG, 80, bao);
			byte[] byteArray = bao.toByteArray();
			
			imgFile = new ParseFile("image.jpg", byteArray);
			imgFile.saveInBackground(imgUpFinishedCallback);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// Upload thumb to parse
	private void uploadThumb() {
		try {
			thumb = ThumbnailUtils.extractThumbnail(((BitmapDrawable) img.getDrawable()).getBitmap(), 75, 75);
			ByteArrayOutputStream thumbBao = new ByteArrayOutputStream();
			thumb.compress(Bitmap.CompressFormat.JPEG, 100, thumbBao);
			byte[] thumbByteArray = thumbBao.toByteArray();
			
			thumbFile = new ParseFile("thumb.jpg", thumbByteArray);
			thumbFile.saveInBackground(thumbUpFinishedCallback);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// Commit Parse object with image, thumb and information to parse
	private void commitParseObject() {		
		ParseObject imageUpload = new ParseObject("UserPhoto");
		imageUpload.put("imageName", venueEditText.getText().toString());
		imageUpload.put("userName", nameEditText.getText().toString());
		imageUpload.put("email", emailEditText.getText().toString());
		imageUpload.put("comment", commentEditText.getText().toString());
		imageUpload.put("locked", "F");
		imageUpload.put("imageFile", imgFile);
		imageUpload.put("thumbFile", thumbFile);
		
		imageUpload.saveInBackground(objectUpFinishedCallback);
	}
	
	// Create bitmap for review
	private Bitmap createBitmapFromURI(String uri) {
//		Log.v(TAG, "uri: " + uri);
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(uri, options);
		
		int height = options.outHeight;
		int width = options.outWidth;
		
		int dstWidth;
		int dstHeight;
		
		if(height > width) {
			dstHeight = 960;
			dstWidth = 640;
		}
		else {
			dstHeight = 640;
			dstWidth = 960;
		}
		
		options.inJustDecodeBounds = false;
		options.inSampleSize = calculteInSampleSize(options, dstWidth, dstHeight);
		
		return BitmapFactory.decodeFile(uri, options);
	}
	
	private int calculteInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		
//		Log.v(TAG, "Height: " + height + " - Width: " + width);
		int inSampleSize = 1;
		
		if(height > reqHeight || width > reqWidth) {
			final int heightRatio = (int) Math.ceil((float) height / (float) reqHeight);
			final int widthRatio = (int) Math.ceil((float) width / (float) reqWidth);
			
//			Log.v(TAG, "Height ratio: " + heightRatio);
//			Log.v(TAG, "Width ratio: " + widthRatio);
			
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}
		
//		Log.v(TAG, "inSample: " + inSampleSize);
		return inSampleSize;
	}
	
	private String getRealPathFromURI(Uri contentUri) {
	    String res = null;
	    String[] proj = { MediaStore.Images.Media.DATA };
	    Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
	    if(cursor.moveToFirst()){;
	       int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	       res = cursor.getString(column_index);
	    }
	    cursor.close();
	    return res;
	}
	
	@Override
	protected void onDestroy() {
		if(null != ((BitmapDrawable) img.getDrawable()))
			((BitmapDrawable) img.getDrawable()).getBitmap().recycle();
		
		if(null != thumb)
			thumb.recycle();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		if(formViewShown) {
			vf.showPrevious();
			formViewShown = false;
		}
		else
			super.onBackPressed();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case IMAGE_TAKEN:
			if(resultCode == RESULT_OK) {
//				Log.v("IMAGE_TAKEN", "Path: " + storageDir.getPath());
				
				img.setImageBitmap(createBitmapFromURI(storageDir.getPath()));
				vf.showNext();
//				Log.v("FORM", "Requesting focus");
				venueEditText.setFocusable(true);
				venueEditText.requestFocus();
				formViewShown = true;
			}
			else {
				
			}
			break;
			
		case IMAGE_PICKED:
			if(resultCode == RESULT_OK) {
//				Log.v("IMAGE_PICKED", "Path: " + getRealPathFromURI(data.getData()));
				
				img.setImageBitmap(createBitmapFromURI(getRealPathFromURI(data.getData())));
				vf.showNext();
				formViewShown = true;
			}
			else {
				
			}
			break;

		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.upload, menu);
//		return true;
//	}

}
