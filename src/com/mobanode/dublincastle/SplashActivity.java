package com.mobanode.dublincastle;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import com.mobanode.castletownhouse.R;

public class SplashActivity extends Activity {

	private static Activity activity;
//	private static long startTime;
//	private static long loadingTime;
//	private static boolean isDownloadStarted;
	
	private static final long SPLASH_DELAY_TIME = 3000;
	private static Handler splashDelayHandler;
	private static Runnable runnable;
	
//	private static AppPrefs prefs;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		
		activity = this;
		
		leaveSplashScreen();
	}
	
	/**
	 * Leave splash screen. Depends on time taken to download feed.
	 */
	public static void leaveSplashScreen() {
		splashDelayHandler = new Handler();
		runnable = new Runnable() {
			
			public void run() {
				activity.finish();
				activity.startActivity(new Intent(activity, HomeActivity.class));
			}
		};
		
//		if(isDownloadStarted) {
//			loadingTime = System.currentTimeMillis() - startTime;
//			
//			if(loadingTime < SPLASH_DELAY_TIME) {
//				splashDelayHandler.postDelayed(runnable, SPLASH_DELAY_TIME - loadingTime);
//			} else {
//				splashDelayHandler.post(runnable);
//			}
//		}
//		else {
			splashDelayHandler.postDelayed(runnable, SPLASH_DELAY_TIME);
//		}
	}
	
	@Override
	protected void onDestroy() {
		unbindDrawables(findViewById(R.id.splash_root_layout));
		System.gc();
		super.onDestroy();
	}
	
	private void unbindDrawables(View view) {
	    if (view.getBackground() != null) {
	    	view.getBackground().setCallback(null);
	    }
	    if (view instanceof ViewGroup) {
	        for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
	        	unbindDrawables(((ViewGroup) view).getChildAt(i));
	        }
	        ((ViewGroup) view).removeAllViews();
	    }
	}

}
