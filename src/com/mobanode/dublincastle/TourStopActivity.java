package com.mobanode.dublincastle;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.mobanode.castletownhouse.R;
import com.mobanode.dublincastle.adapters.GalleryAdapter;
import com.mobanode.prefs.AppPrefs;
import com.mobanode.utils.AppConstants;
import com.mobanode.utils.CannedInfoArrays;
import com.mobanode.utils.DublinCastleActivity;
import com.mobanode.utils.MobaWebViewClient;

@SuppressWarnings("deprecation")
public class TourStopActivity extends DublinCastleActivity {

	private Gallery gallery;
	private GalleryAdapter adapter;
	
	private final int GALLERY_SLIDE_INTERVAL = 6000;
	
	private int picPosition;
	private Handler galleryHandler = new Handler();
	
	private AppPrefs prefs = KilkennyCastleApplication.getPrefsInstance();
	
	private WebView webView;
	
	private final String IRISH_URL_SUFFIX = "_ie.html";
	private final String ENGLISH_URL_SUFFIX = "_en.html";
	
	private String tag;
	
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tour_stop);
		
		gallery = (Gallery) findViewById(R.id.tour_stop_gallery);
		webView = (WebView) findViewById(R.id.tour_stop_web_view);
		
		tag = getIntent().getStringExtra("tag");
		
		if(tag != null) {
			initGallery(tag);
			
			setTitle(getTourStopTitle(tag));
			
//			if(tag.equals("island_info")) {
//				((RelativeLayout) findViewById(R.id.tour_title_layout)).setVisibility(View.GONE);
//			}
//			else {
//				((TextView) findViewById(R.id.tour_stop_title_text_view)).setText(getTourStopTitle(tag));
//			}
//			
			if(prefs.viewInIrish()) {
				webView.loadUrl(AppConstants.ASSETS_HTML_FOLDER_PATH + tag + IRISH_URL_SUFFIX);
		//		((Button) findViewById(R.id.tour_show_on_map_btn)).setText(R.string.action_map_map_view_ie);
			}
			else {
				webView.loadUrl(AppConstants.ASSETS_HTML_FOLDER_PATH + tag + ENGLISH_URL_SUFFIX);
			}
		}
		
		webView.setWebViewClient(new MobaWebViewClient(this));
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setNeedInitialFocus(false);
		webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
	}
	
	public void mapBtnClick(View view) {
		Intent intent = new Intent(this, MapsActivity.class);
		intent.putExtra("mapType", "single");
		intent.putExtra("tag", tag);
		startActivity(intent);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if(prefs.isMusicPlaying()){
			getMenuInflater().inflate(R.menu.audio_menu, menu);
		}
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_audio:
			if(prefs.isMusicPlaying()){
				pauseAudio();
				item.setIcon(R.drawable.pause_white);
			}else{
				playAudio();
				item.setIcon(R.drawable.pause_white);
			}
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
//	private class MobaWebViewClient extends WebViewClient {
//
//		@Override
//		public boolean shouldOverrideUrlLoading(WebView view, String url) {
//			if(url.startsWith("tel:")) {
//				startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(url)));
//			}
//			else if(url.startsWith("http")) {
//				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
//			}
//			else if(url.startsWith("sms")) {
//				Intent smsIntent = new Intent(Intent.ACTION_SENDTO);
//				smsIntent.setData(Uri.parse(url));
//				startActivity(Intent.createChooser(smsIntent, "Send"));
//			}
//			else if(url.startsWith("mailto")) {
//				MailTo mt = MailTo.parse(url);
//				Intent emailIntent = new Intent(Intent.ACTION_SEND);
//				emailIntent.setType("text/plain");
//				emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{mt.getTo()});
//				startActivity(Intent.createChooser(emailIntent, "Send"));//emailIntent);
//			}
//			else {
//				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
//			}
//			return true;
//		}
//	}
	
	private Runnable runnable = new Runnable() {
		
		@Override
		public void run() {
			scrollSlideShow();
			galleryHandler.postDelayed(runnable, GALLERY_SLIDE_INTERVAL);
		}
	};
	
	private String getTourStopTitle(String tag) {
		if(tag.equals("island_info")) {
			if(prefs.viewInIrish())
				return getResources().getString(R.string.title_activity_island_info_ie);
			else
				return getResources().getString(R.string.title_activity_island_info);
		}
		else {
			for(int i = 0; i < AppConstants.TOUR_IMAGES_TAGS.length; i++) {
				if(tag.equals(AppConstants.TOUR_IMAGES_TAGS[i])) {
					if(prefs.viewInIrish())
						return getResources().getStringArray(R.array.tour_stops_ie)[i];
					else
						return getResources().getStringArray(R.array.tour_stops_en)[i];
				}
			}
			
			if(prefs.viewInIrish())
				return getResources().getString(R.string.title_activity_tour_ie);
			else
				return getResources().getString(R.string.title_activity_tour);
		}
	}
	
	private void initGallery(String tag) {
		adapter = new GalleryAdapter(this, CannedInfoArrays.getTourImageList(tag));
		gallery.setAdapter(adapter);
		
		final int count = adapter.getCount();
		
		if(count > 1)
		{
			final LinearLayout layout = (LinearLayout) findViewById(R.id.festival_dots);
			
			for (int i = 0; i < count; i++)
			{
				ImageView v = new ImageView(this);
	    		v.setPadding(8, 0, 8, 8);

	    		v.setImageDrawable(getResources().getDrawable(R.drawable.gallery_dot));
	    		layout.addView(v);
			}
			
			gallery.setOnItemSelectedListener(new OnItemSelectedListener()
			{
	    		
	    		public void onItemSelected(AdapterView<?> parent, View view,
	    				int pos, long id) {
	    			for (int i = 0; i < count; i++) {
	    				ImageView v = (ImageView)layout.getChildAt(i);
	    				if (i == pos) {
	    					v.setAlpha(255);
	    				} else {
	    					v.setAlpha(100);
	    				}
	    			}
	    			
	    		}
	    		
	    		public void onNothingSelected(AdapterView<?> arg0) { }
	    	});
			
			galleryHandler.postDelayed(runnable, GALLERY_SLIDE_INTERVAL);
		}
	}
	
	/**
	 * Scroll Gallery to next image
	 */
	private void scrollSlideShow() {
		picPosition = gallery.getSelectedItemPosition() + 1;
		
		if(picPosition >= adapter.getCount())
			picPosition = 0;
		
		gallery.setSelection(picPosition, true);
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		if(!tag.equals("island_info")) {
//			getMenuInflater().inflate(R.menu.tour_stop, menu);
//			
//			if(prefs.viewInIrish())
//				menu.getItem(0).setTitle(R.string.action_map_show_on_map_ie);
//		}
//
//		return true;
//	}
	
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		switch (item.getItemId()) {
//		case R.id.action_view_tour_on_map:
//			Intent intent = new Intent(this, MapsActivity.class);
//			intent.putExtra("mapType", "single");
//			intent.putExtra("tag", tag);
//			startActivity(intent);
//			break;
//
//		default:
//			break;
//		}
//		return super.onOptionsItemSelected(item);
//	}

}
