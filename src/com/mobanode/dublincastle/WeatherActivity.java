package com.mobanode.dublincastle;

import java.text.DecimalFormat;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import com.mobanode.castletownhouse.R;
import com.mobanode.entities.WeatherForecast;
import com.mobanode.utils.AppConstants;
import com.mobanode.utils.CommonUtils;

@SuppressLint("SetJavaScriptEnabled")
public class WeatherActivity extends Activity {
	
	private WebView webView;
	
	private TextView summary;
	private TextView temperature;
	
	private ImageView weatherIcon;
	
	private final String WEBVIEW_CSS = "<style type=\"text/css\">" +
											"img {width:100%; height:auto;}" +
											"body{background:#c7b299; margin:0px;}" +
											"p.dateText{width:100%; text-align:center;}" +
										"</style>";
	
	private final String DEGREE_UNICODE = "\u00B0";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_weather);
		
		if(KilkennyCastleApplication.getPrefsInstance().viewInIrish())
			setTitle(R.string.title_activity_weather_ie);
		
		webView = (WebView) findViewById(R.id.webcam_webview);
		
		summary = (TextView) findViewById(R.id.forecast_summary);
		temperature = (TextView) findViewById(R.id.forecast_temperature);
		
		weatherIcon = (ImageView) findViewById(R.id.weather_icon_image_view);
		
		new DownloadWebcamPageHTML().execute();
		new DownloadWeatherForecastJSON().execute();
	}
	
	/**
	 * Load HTML String with custom CSS into WebView
	 * @param html - HTML response as String
	 */
	private void loadWebviewData(String html) {
		html = html.replace("<br />", "");
		webView.loadDataWithBaseURL(null, WEBVIEW_CSS + html, "text/html", "UTF-8", null);
		
		webView.setWebViewClient(new WebViewClient());
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setNeedInitialFocus(false);
		webView.getSettings().setBuiltInZoomControls(false);
		webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
	}
	
	
	/**
	 * Display current weather
	 * @param forecast - Forecast object containing info
	 */
	private void displayWeatherForecast(WeatherForecast forecast) {
		summary.setText(forecast.getSummary());
		temperature.setText(roundTempDegrees(forecast.getTemperature()) + DEGREE_UNICODE);
		weatherIcon.setImageDrawable(CommonUtils.loadWeatherImageFromDrawables(this, forecast.getIcon()));
	}
	
	/**
	 * Round temperature degrees to 1 decimal place
	 * @param degrees
	 * @return
	 */
	private double roundTempDegrees(double degrees) {
		DecimalFormat df = new DecimalFormat("##.#");
		return Double.valueOf(df.format(degrees));
	}
	
//	private Handler displayHandler = new Handler();
//	
//	private Runnable displayRunnable = new Runnable() {
//		
//		@Override
//		public void run() {
////			displayWeatherForecast()
//		}
//	};
	
	
	/**
	 * AsyncTask to download the Webcam HTML
	 * @author mikenolan
	 *
	 */
	private class DownloadWebcamPageHTML extends AsyncTask<Void, Void, String> {
		
		@Override
		protected void onPostExecute(String result) {
			if(result.equals("error")) {
//				loadWebviewData(result);
			}
			else
				loadWebviewData(result);
			
			super.onPostExecute(result);
		}

		@Override
		protected String doInBackground(Void... params) {
			return downloadHTML(AppConstants.WEBCAM_URL);
		}
		
	}
	
	
	/**
	 * AsyncTask to download the weather feed JSON
	 * @author mikenolan
	 *
	 */
	private class DownloadWeatherForecastJSON extends AsyncTask<Void, Void, WeatherForecast> {
		
		@Override
		protected void onPostExecute(WeatherForecast result) {
			if(result != null)
				displayWeatherForecast(result);
			super.onPostExecute(result);
		}

		@Override
		protected WeatherForecast doInBackground(Void... params) {
			String json = downloadHTML(AppConstants.FORECAST_URL);

			if(json.equals("error"))
				return null;
			else
				return new WeatherForecast().parserJson(json);
		}
		
	}
	
	
	/**
	 * Create a String value of the response from a URL call
	 * @param url - URL to be downloaded as a String
	 * @return - Response as a String if successful / "error" if failed
	 */
	private String downloadHTML(String url)
	{
		try
		{
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(url);
			HttpResponse response = httpClient.execute(httpGet);
			
			return EntityUtils.toString(response.getEntity());
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return "error";
		}
	}
	
//	private String getimgTagFromHTML(String html) {
//		Pattern p = Pattern.compile("<img[^>]+src\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>");
//		Matcher m = p.matcher(html);
//		
//		if (m.find()) {
//			return m.group();
//		}
//		else {
//			return "";
//		}
//	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.weather, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_weather_refresh:
			new DownloadWebcamPageHTML().execute();
			new DownloadWeatherForecastJSON().execute();
			break;

		default:
			break;
		}
		return false;
	}

}
