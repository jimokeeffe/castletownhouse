package com.mobanode.dublincastle;

import java.net.URLEncoder;
import java.util.ArrayList;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.mobanode.castletownhouse.R;
import com.mobanode.dublincastle.adapters.FacebookListAdapter;
import com.mobanode.dublincastle.adapters.TwitterListAdapter;
import com.mobanode.dublincastle.adapters.YouTubeListAdapter;
import com.mobanode.entities.FacebookPost;
import com.mobanode.entities.Tweet;
import com.mobanode.entities.YoutubePost;
import com.mobanode.utils.DataCache;

public class SocialActivity extends Activity {

	private ListView socialListView;
	private ProgressDialog mProgress;
	private Button twitBtn;
	private Button fbBtn;
	private Button youtubeBtn;
	private ProgressBar progBar;
	private final int SOCIAL_DIALOG = 1;
	
	/* Twitter */
	private String tweetID;
	
	/* Facebook */
	private boolean fbFeed = false;
	private boolean ytFeed = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_social);
		
		twitBtn = (Button) findViewById(R.id.twitter_btn);
		fbBtn = (Button) findViewById(R.id.facebook_btn);
		youtubeBtn = (Button) findViewById(R.id.youtube_btn);
		socialListView = (ListView) findViewById(R.id.social_list_view);
		
		socialListView.setOnItemClickListener(socialListClickListener);
		
		twitterBtnClick(twitBtn);
	}
	
	private OnItemClickListener socialListClickListener = new OnItemClickListener() {

		@SuppressWarnings("deprecation")
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			if(fbFeed) {
				if(!DataCache.facebookPosts.get(position).getLink().equals(""))
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(DataCache.facebookPosts.get(position).getLink())));
			}
			else if(ytFeed) {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(DataCache.youtubePosts.get(position).getVidUrl())));
			}
			else {
				tweetID = DataCache.twitterPosts.get(position).getTweetId();
				showDialog(SOCIAL_DIALOG);
			}
		}
	};
	
	public void displayPosts() {
		if(fbFeed)
			socialListView.setAdapter(new FacebookListAdapter(this, R.layout.list_item_facebook, DataCache.facebookPosts, DataCache.fbImgUrls));
		else if(ytFeed)
			socialListView.setAdapter(new YouTubeListAdapter(this, R.layout.list_item_youtube, DataCache.youtubePosts));
		else
			socialListView.setAdapter(new TwitterListAdapter(this, R.layout.list_item_twitter, DataCache.twitterPosts));
	}
	
	/* Twitter Button click */
	public void twitterBtnClick(View view) {
		view.setEnabled(false);
		fbBtn.setEnabled(true);
		youtubeBtn.setEnabled(true);
		
		fbFeed = false;
		ytFeed = false;
		
		if(DataCache.twitterPosts == null)
			new SocialFeedAsync().execute();
		else
			displayPosts();
	}
	
	/* Facebook Button click */
	public void facebookBtnClick(View view) {
		view.setEnabled(false);
		twitBtn.setEnabled(true);
		youtubeBtn.setEnabled(true);
		
		fbFeed = true;
		ytFeed = false;
		
		if(DataCache.facebookPosts == null)
			new SocialFeedAsync().execute();
		else
			displayPosts();
	}
	
	/* YouTube Button click */
	public void youtubeBtnClick(View view) {
		view.setEnabled(false);
		twitBtn.setEnabled(true);
		fbBtn.setEnabled(true);
		ytFeed = true;
		fbFeed = false;
		
		if(DataCache.youtubePosts == null)
			new SocialFeedAsync().execute();
		else
			displayPosts();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.social, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(fbFeed) {
			if(DataCache.facebookPosts == null)
				DataCache.facebookPosts = new ArrayList<FacebookPost>();
			DataCache.facebookPosts.clear();
		}
		else if(ytFeed) {
			if(DataCache.youtubePosts == null)
				DataCache.youtubePosts = new ArrayList<YoutubePost>();
			DataCache.youtubePosts.clear();
		}
		else {
			if(DataCache.twitterPosts == null)
				DataCache.twitterPosts = new ArrayList<Tweet>();
			DataCache.twitterPosts.clear();
		}
		
		switch (item.getItemId()) {
		case R.id.menu_refresh_feed:
			new SocialFeedAsync().execute();
			break;

		default:
			break;
		}
		return false;
	}
	
	
	
	/********************************
	 * 
	 * Create dialog to show Tweet
	 * 
	 ********************************/
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog dialog;

		switch (id) {
		case SOCIAL_DIALOG:
			dialog = new Dialog(SocialActivity.this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setOnCancelListener(new OnCancelListener() {

				@SuppressWarnings("deprecation")
				public void onCancel(DialogInterface dialog) {
					removeDialog(SOCIAL_DIALOG);
				}
			});
			
			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final View view = inflater.inflate(R.layout.dialog_layout_social, (ViewGroup) findViewById(R.id.socialWebDialog));
			dialog.setContentView(view);

			progBar = (ProgressBar) view.findViewById(R.id.loadPostProgressBar);

			WebView socWebView = (WebView) view.findViewById(R.id.socialWebView);
			socWebView.getSettings().setJavaScriptEnabled(true);
			socWebView.getSettings().setDomStorageEnabled(true);
			socWebView.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17");
			socWebView.setScrollBarStyle(WebView.SCROLLBARS_INSIDE_OVERLAY);

			socWebView.setWebViewClient(new WebViewClient() {

				@Override
				public void onPageFinished(WebView view, String url) {
					progBar.setVisibility(ProgressBar.GONE);
				}

				@Override
				public boolean shouldOverrideUrlLoading(WebView view, String url) {
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
					return true;
				}
			});
			
			socWebView.loadUrl(Tweet.TWEET_URL + tweetID);
			break;

		default:
			dialog = null;
		}
		return dialog;
	}
	
	
	
	
	/*****************************************************************
	 * 
	 * AsyncTask to download Twitter, YouTube or Facebook posts 
	 * 
	 ****************************************************************/
	public class SocialFeedAsync extends AsyncTask<Void, Void, Void> {
		String strResponse = "";
		
		@Override
		protected void onPreExecute() {
			if(fbFeed)
				mProgress = ProgressDialog.show(SocialActivity.this, "Please wait...", "Downloading Facebook Posts", true, true);
			else if(ytFeed)
				mProgress = ProgressDialog.show(SocialActivity.this, "Please wait...", "Downloading YouTube Feed", true, true);
			else
				mProgress = ProgressDialog.show(SocialActivity.this, "Please wait...", "Downloading Tweets", true, true);
			mProgress.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			if (fbFeed)
				getFacebookFeed();
			else if(ytFeed)
				getYoutubeFeed();
			else
				getTwitterFeed();
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			mProgress.dismiss();
			displayPosts();
		}

		private void getTwitterFeed() {
			Tweet.parseTwitterJson(Tweet.getNewTwitterFeed());
//			try {
//				HttpClient httpClient = new DefaultHttpClient();
//				HttpGet httpGet = new HttpGet("http://api.twitter.com/1/statuses/user_timeline.json?screen_name=" + Tweet.TWITTER_USER_NAME + "&count=20&include_rts=1");
//				HttpResponse response = httpClient.execute(httpGet);
//				strResponse = EntityUtils.toString(response.getEntity());
//
//				Tweet.parseTwitterJson(strResponse);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
		}

		private void getFacebookFeed() {
			try {
				String accessTokenURL = "https://graph.facebook.com/oauth/access_token?client_id=" 	+ FacebookPost.APP_ID + "&client_secret=" + FacebookPost.APP_SECRET + "&grant_type=client_credentials&limit=25";
				HttpClient httpClient = new DefaultHttpClient();
				HttpGet httpGet = new HttpGet(accessTokenURL);
				HttpResponse response = httpClient.execute(httpGet);
				String accessToken = EntityUtils.toString(response.getEntity());
				accessToken = accessToken.split("=")[1];

				String fbURL = "https://graph.facebook.com/" + FacebookPost.PAGE_ID + "/posts?access_token=" + URLEncoder.encode(accessToken, "UTF-8");
				httpGet = new HttpGet(fbURL);
				response = httpClient.execute(httpGet);
				String jsonRes = EntityUtils.toString(response.getEntity());
				
				FacebookPost.parseFBJson(jsonRes);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		private void getYoutubeFeed() {
			try {
				String ytUrl = "http://gdata.youtube.com/feeds/api/users/stpatricksfest2011/favorites?&v=2&max-results=24&alt=jsonc";
				HttpClient httpClient = new DefaultHttpClient();
				HttpGet httpGet = new HttpGet(ytUrl);
				HttpResponse response = httpClient.execute(httpGet);
				strResponse = EntityUtils.toString(response.getEntity());
				
				YoutubePost.parseYoutubeJSON(strResponse);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}

}
