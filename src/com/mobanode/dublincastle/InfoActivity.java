package com.mobanode.dublincastle;



import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;
import android.widget.TextView;
import com.mobanode.castletownhouse.R;
import com.mobanode.dublincastle.carousel.CarouselInfoPagerAdapter;
import com.mobanode.prefs.AppPrefs;
import com.mobanode.resourcescommon.imgviewresize.ResizableImageView;
import com.mobanode.utils.DublinCastleFragmentActivity;

public class InfoActivity extends DublinCastleFragmentActivity {

	public static ViewPager infoPager;
	
	private CarouselInfoPagerAdapter adapter;
	private AppPrefs prefs;
	private FragmentManager fm;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_info);
		
		fm = this.getSupportFragmentManager();
		
		infoPager = (ViewPager) findViewById(R.id.info_viewpager);
		TextView textView = (TextView) findViewById(R.id.info_title_textview);
		adapter = new CarouselInfoPagerAdapter(fm, this, textView);
		infoPager.setOnPageChangeListener(adapter);
		
		infoPager.setAdapter(adapter);
		infoPager.setCurrentItem(0);
		infoPager.setOffscreenPageLimit(3);
		
		if(KilkennyCastleApplication.getPrefsInstance().viewInIrish()) {
			((ResizableImageView) findViewById(R.id.dublin_castle_header)).setImageDrawable(getResources().getDrawable(R.drawable.dublin_castle_header));
			setTitle(R.string.title_activity_info_ie);
		}
		
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_audio:
			if(prefs.isMusicPlaying()){
				pauseAudio();
				item.setTitle("Play");
				item.setIcon(R.drawable.play_white);
			}else{
				playAudio();
				item.setTitle("Pause");
				item.setIcon(R.drawable.pause_white);
			}
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

}
