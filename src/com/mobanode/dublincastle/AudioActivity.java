package com.mobanode.dublincastle;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import com.mobanode.castletownhouse.R;
import com.mobanode.prefs.AppPrefs;
import com.mobanode.resourcescommon.imgviewresize.ResizableImageView;
import com.mobanode.utils.AppConstants;
import com.mobanode.utils.DublinCastleActivity;

public class AudioActivity extends DublinCastleActivity {

	// private PlayAudio service;
	private final String AUDIO_EN = "audio_en.html";
	private final String AUDIO_IE = "audio_ie.html";
	private AppPrefs prefs = KilkennyCastleApplication.getPrefsInstance();
	private ImageButton playPauseButton;
	private ImageButton stopButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_audio);
		
		WebView webView = (WebView) findViewById(R.id.audio_web_view);
		playPauseButton = (ImageButton) findViewById(R.id.play_audio_button);
		stopButton = (ImageButton) findViewById(R.id.stop_audio_button);
		
		if(prefs.viewInIrish()) {
			setTitle(R.string.title_activity_tour_ie);
			webView.loadUrl(AppConstants.ASSETS_HTML_FOLDER_PATH + AUDIO_IE);
			((ResizableImageView) findViewById(R.id.home_info_button)).setImageDrawable(getResources().getDrawable(R.drawable.tour_button_ie));
		}
		else{
			webView.loadUrl(AppConstants.ASSETS_HTML_FOLDER_PATH + AUDIO_EN);
		}
		setAudioButton();
	}

	private void setAudioButton(){
		if(isPlaying()){
			playPauseButton.setImageDrawable(getResources().getDrawable(R.drawable.pause_audio_semi));
			
			
			
		}
		else{
			playPauseButton.setImageDrawable(getResources().getDrawable(R.drawable.play_audio_semi));
		}
		
	}
	public void audioControlClick(View view) {
		switch (view.getId()) {
		case R.id.play_audio_button:
			if (isPlaying()) {
				pauseAudio();

			} else {
				playAudio();
			}
			setAudioButton();
			break;

		case R.id.stop_audio_button:
			startAgain();
			setAudioButton();
			break;

		}
	}

	// @Override
	// protected void onResume() {
	// super.onResume();
	// Intent intent = new Intent(this, PlayAudio.class);
	// bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
	// }
	//
	// private ServiceConnection mConnection = new ServiceConnection() {
	// public void onServiceConnected(ComponentName className, IBinder binder) {
	// PlayAudio.AudioBinder b = (PlayAudio.AudioBinder) binder;
	// service = b.getService();
	// Toast.makeText(AudioActivity.this, "Connected", Toast.LENGTH_SHORT)
	// .show();
	// }
	//
	// public void onServiceDisconnected(ComponentName className) {
	// service = null;
	// }
	// };

	// public void playAudio(View view) {
	// // Intent objIntent = new Intent(this, PlayAudio.class);
	// // startService(objIntent);
	// if (service != null){
	// service.play();
	// }
	// }
	//
	// public void stopAudio(View view) {
	// // Intent objIntent = new Intent(this, PlayAudio.class);
	// // stopService(objIntent);
	// if (service != null){
	// service.pause();
	// }
	// }
}
