package com.mobanode.dublincastle;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.OverlayItem;
import com.mobanode.castletownhouse.R;
import com.mobanode.dublincastle.adapters.MapLocationOverlay;
import com.mobanode.prefs.AppPrefs;
import com.mobanode.utils.DataCache;

public class MapsActivity extends MapActivity implements LocationListener {
	
	private LocationManager locManager;
	private String provider;
	private Location location;
	
	private MapView mapView;
	private MyLocationOverlay mLocationOverlay;
//	private List<GeoPoint> points;
//	private ProgressDialog mProgress;
	
	private Drawable mapPin;
	
//	private double directionsLat = 0.0;
//	private double directionsLng = 0.0;
	
	//private String mapType;
	
	private final AppPrefs prefs = KilkennyCastleApplication.getPrefsInstance();
	
//	private final GeoPoint CASTLE_POINT = new GeoPoint((int)(52.650276 * 1E6), (int)(-7.249174 * 1E6));
	
	private final double lat = 52.650276 ;
	private final double lon = -7.249176 ;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_maps);
		
		if(prefs.viewInIrish())
			setTitle(R.string.title_activity_maps_ie);
		
		mapView = (MapView) findViewById(R.id.mapview);
		mapView.setSatellite(true);
		mapView.setBuiltInZoomControls(true);
		
		mapPin = getResources().getDrawable(R.drawable.map_pin_dublin);// = getResources().getDrawable(R.drawable.map_pin);
		
		mLocationOverlay = new MyLocationOverlay(this, mapView);
		
		mLocationOverlay.runOnFirstFix(new Runnable() {
			public void run() {
				mapView.getOverlays().add(mLocationOverlay);
			}
		});
		
	//	mapType = getIntent().getStringExtra("mapType");
		
		setSingleMapPin(lat, lon, mapPin);
//		new PopulateMap().execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.maps, menu);
		
		if(KilkennyCastleApplication.getPrefsInstance().viewInIrish()) {
			menu.findItem(R.id.mapsLocateMeMenuItem).setTitle(getResources().getString(R.string.action_map_locate_me_ie));
			menu.findItem(R.id.mapsMapViewMenuItem).setTitle(getResources().getString(R.string.action_map_map_view_ie));
//			menu.findItem(R.id.mapsDirectionsMenuItem).setTitle(getResources().getString(R.string.action_map_directions_ie));
		}
		
//		if(mapType.equals("all"))
//			menu.removeItem(R.id.mapsDirectionsMenuItem);
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		
		case R.id.mapsMapViewMenuItem:
			mapView.setSatellite(!mapView.isSatellite());
			
			if(mapView.isSatellite()) {
				if(KilkennyCastleApplication.getPrefsInstance().viewInIrish())
					item.setTitle(getResources().getString(R.string.action_map_map_view_ie));
				else
					item.setTitle(getResources().getString(R.string.action_map_map_view));
			}
			else {
				if(KilkennyCastleApplication.getPrefsInstance().viewInIrish())
					item.setTitle(getResources().getString(R.string.action_map_satellite_view_ie));
				else
					item.setTitle(getResources().getString(R.string.action_map_satellite_view));
			}
			
			break;
			
		case R.id.mapsLocateMeMenuItem:
			mLocationOverlay.runOnFirstFix(new Runnable() {
				public void run() {
//					mapView.getOverlays().add(mLocationOverlay);
					mapView.getController().animateTo(mLocationOverlay.getMyLocation());
				}
			});
			break;
			
//		case R.id.mapsDirectionsMenuItem:
//			if(DataCache.myLocationLat != 0.0 && DataCache.myLocationLng != 0.0 && directionsLat != 0.0 && directionsLng != 0.0) {
//				Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr=" + DataCache.myLocationLat + "," + DataCache.myLocationLng + "&daddr=" + directionsLat + "," + directionsLng));
//				intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
//				startActivity(intent);
//			}
//			break;
		}
		return false;
	}
	
//	private void popuplateMapWithPins() {
//		points = new ArrayList<GeoPoint>();
		
//		if(mapType.equals("all"))
//			setAllPins(mapPin, new  MapLocationParser(this).getMapLocations());
//		else {
//			String tag = getIntent().getStringExtra("tag");
			
//			MapLocation ml = new MapLocationParser(this).getStopLocation(tag);
			
		//	setSingleMapPin(ml.getLatitude(), ml.getLongitude(), prefs.viewInIrish() ? ml.getNameIrish() : ml.getName(), ml.getTag(), mapPin);
//		}
//}
	
	private void setSingleMapPin(double lat, double lon, Drawable drawable) {
//		directionsLat = lat;
//		directionsLng = lon;
		
		GeoPoint gp = new GeoPoint((int)(lat * 1E6), (int)(lon * 1E6));
		OverlayItem item = new OverlayItem(gp, "", "");
		
		MapLocationOverlay overlay = new MapLocationOverlay(drawable, this, mapView, false);
		overlay.addOverlay(item);
		
		// Max Zoom = 22
		mapView.getController().setZoom(16);
		mapView.getController().setCenter(gp);
		
		mapView.getOverlays().add(overlay);
	}
	
//	private void setAllPins(Drawable drawable, ArrayList<MapLocation> locations) {
//		MapLocationOverlay overlay = new MapLocationOverlay(drawable, this, mapView, true);
//		
//		for(MapLocation ml : locations) {
//			GeoPoint gp = new GeoPoint((int)(ml.getLatitude() * 1E6), (int)(ml.getLongitude() * 1E6));
//			points.add(gp);
//			OverlayItem item = new OverlayItem(gp, prefs.viewInIrish() ? ml.getNameIrish() : ml.getName(), ml.getTag());
//			
//			overlay.addOverlay(item);
//		}
//		
//		// Visitor Centre Pin
//		points.add(CASTLE_POINT);
//		OverlayItem centreItem = new OverlayItem(CASTLE_POINT, "Vistor Centre", "");
//		
//		MapLocationOverlay visitorCentreOverlay = new MapLocationOverlay(drawable, this, mapView, false);
//		visitorCentreOverlay.addOverlay(centreItem);
//		
//		MapUtils.zoomMap(points, mapView);
//		
//		mapView.getOverlays().add(overlay);
//		mapView.getOverlays().add(visitorCentreOverlay);
//	}
//	
//	private class PopulateMap extends AsyncTask<Void, Void, Void> {
//
//		@Override
//		protected void onPreExecute() {
//			mProgress = ProgressDialog.show(MapsActivity.this, "", getResources().getString(R.string.please_wait_label));
//			super.onPreExecute();
//		}
//		
//		@Override
//		protected Void doInBackground(Void... params) {
//			popuplateMapWithPins();
//			return null;
//		}
//		
//		@Override
//		protected void onPostExecute(Void result) {
//			mProgress.dismiss();
//			super.onPostExecute(result);
//		}
//		
//	}
	
	private void initLocationListener() {
		locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		
		Criteria criteriaFine = new Criteria();
		criteriaFine.setAccuracy(Criteria.ACCURACY_FINE);
		String fineProvider = locManager.getBestProvider(criteriaFine, true);
		if(fineProvider != null)
			locManager.requestLocationUpdates(fineProvider, 250, 5, this);
		
		Criteria criteriaCourse = new Criteria();
		criteriaCourse.setAccuracy(Criteria.ACCURACY_COARSE);
		String coarseProvider = locManager.getBestProvider(criteriaCourse, true);
		if(coarseProvider != null)
			locManager.requestLocationUpdates(coarseProvider, 250, 5, this);
		
		Criteria criteria = new Criteria();
		provider = locManager.getBestProvider(criteria, false);
		location = locManager.getLastKnownLocation(provider);
		
		if(location != null)
			onLocationChanged(location);
	}
	
	@Override
	protected void onDestroy() {
		locManager.removeUpdates(this);
		mLocationOverlay.disableMyLocation();
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		locManager.removeUpdates(this);
		mLocationOverlay.disableMyLocation();
		super.onPause();
	}

	@Override
	protected void onResume() {
		mLocationOverlay.enableMyLocation();
		initLocationListener();
		super.onResume();
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	@Override
	public void onLocationChanged(Location location) {
		this.location = location;
		DataCache.myLocationLat = location.getLatitude();
		DataCache.myLocationLng = location.getLongitude();
	}

	@Override
	public void onProviderDisabled(String provider) {}

	@Override
	public void onProviderEnabled(String provider) {}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {}

}
