package com.mobanode.dublincastle;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.mobanode.castletownhouse.R;
import com.mobanode.prefs.AppPrefs;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.model.ImageTag;
import com.novoda.imageloader.core.model.ImageTagFactory;

public class GalleryImageActivity extends Activity {

	private AppPrefs prefs = KilkennyCastleApplication.getPrefsInstance();
	
	// Fling Gesture constants
	private static final int SWIPE_MIN_DISTANCE = 5;
	private static final int SWIPE_MAX_OFF_PATH = 250;
	private static final int SWIPE_THRESHOLD_VELOCITY = 5;
	private GestureDetector gestureDetector;
		
	private ImageTagFactory imgTagFactory;
	private ImageManager imageLoader = KilkennyCastleApplication.getImageManager();
	
	private ImageView imageView;
	private int position;
	
	private String venueLabel;
	private String commentLabel;
	
	private TextView venueTextView;
	private TextView commentTextView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gallery_image);
		
		imageView = (ImageView) findViewById(R.id.gallery_image_view);
		venueTextView = (TextView) findViewById(R.id.gallery_venue_text_view);
		commentTextView = (TextView) findViewById(R.id.gallery_comment_text_view);
		
		if(prefs.viewInIrish()) {
			setTitle(getResources().getString(R.string.title_activity_gallery_ie));
			venueLabel = getResources().getString(R.string.gallery_venue_label_ie);
			commentLabel = getResources().getString(R.string.gallery_comment_label_ie);
		}
		else {
			setTitle(getResources().getString(R.string.title_activity_gallery));
			venueLabel = getResources().getString(R.string.gallery_venue_label);
			commentLabel = getResources().getString(R.string.gallery_comment_label);
		}
		
		this.imgTagFactory = ImageTagFactory.newInstance(this, R.drawable.grid_holder);
		this.imgTagFactory.setErrorImageId(R.drawable.grid_holder);
		
		if(savedInstanceState != null) {
			position = savedInstanceState.getInt("pos", -1);
			
			if(position == -1)
				position = getIntent().getIntExtra("arrPos", -1);
		}
		else
			position = getIntent().getIntExtra("arrPos", -1);
		
		setImageGestureListener();
		
		changeImage();
		changeImageText();
	}
	
	private void setImageGestureListener() {
		gestureDetector = new GestureDetector(this, new SwipeGestureListener());
		
		imageView.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (gestureDetector.onTouchEvent(event)) {
					return true;
				}
				return false;
			}
		});
	}
	
	public void goToNextImage(View view) {
		if(position == GalleryActivity.fullResUrls.size() - 1)
			position = 0;
		else
			position ++;
		
		changeImageText();
		changeImage();
	}
	
	public void goToPreviousImage(View view) {
		if(position == 0)
			position = GalleryActivity.fullResUrls.size() - 1;
		else
			position --;
		
		changeImageText();
		changeImage();
	}
	
	private void changeImage() {
		imageView.setImageResource(android.R.color.transparent);
		
		String url = GalleryActivity.fullResUrls.get(position);
		
		ImageTag imgTag = imgTagFactory.build(url, this);
		imageView.setTag(imgTag);
		imageLoader.getLoader().load(imageView);
	}
	
	private void changeImageText() {
		if(!GalleryActivity.venueStrings.get(position).equals(""))
			venueTextView.setText(venueLabel + " " + GalleryActivity.venueStrings.get(position));
		else
			venueTextView.setText("");
		
		if(!GalleryActivity.commentStrings.get(position).equals(""))
			commentTextView.setText(commentLabel + " " + GalleryActivity.commentStrings.get(position));
		else
			commentTextView.setText("");
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putInt("pos", position);
		super.onSaveInstanceState(outState);
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.gallery_image, menu);
//		return true;
//	}
	
	// Swipe Gesture listener class
		public class SwipeGestureListener extends SimpleOnGestureListener {

			@Override
			public boolean onDown(MotionEvent e) {
//				Log.v("SWIPE", "ON DOWN-----------------------------");
				return true;
			}

			@Override
			public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
//				Log.v("SWIPE", "e1-X: " + e1.getX());
//				Log.v("SWIPE", "e1-Y: " + e1.getY());
//				Log.v("SWIPE", "e2-X: " + e2.getX());
//				Log.v("SWIPE", "e2-Y: " + e2.getY());
//				Log.v("SWIPE", "velX: " + velocityX);
//				Log.v("SWIPE", "velY: " + velocityY);
//				Log.v("SWIPE", "-----------------------------");

				if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH) {
					Log.v("SWIPE", "First if - Swipe off path");
					return false;
				}

				// Swipe Right to Left (<--)
				if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					goToPreviousImage(null);
				}
				// Swipe Left to Right (-->)
				else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					goToNextImage(null);
				}

				return false;
			}

		}

}
