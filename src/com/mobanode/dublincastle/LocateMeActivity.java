package com.mobanode.dublincastle;

import java.text.DecimalFormat;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;
import com.mobanode.castletownhouse.R;
import com.mobanode.utils.DialogUtils;

public class LocateMeActivity extends Activity implements LocationListener {

	private final String EMERGENCY_CONTACTS_URL = "http://www.mobanode.mobi/blaskets/emergency_contacts.html";
	private final String EMERGENCY_CONTACTS_URL_IE = "http://www.mobanode.mobi/blaskets/emergency_contacts_ie.html";
	
	private LocationManager locManager;
	private String provider;
	private Location location;
	
	private WebView webView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_locate_me);
		
		webView = (WebView) findViewById(R.id.emergency_contacts_webview);
		
		if(KilkennyCastleApplication.getPrefsInstance().viewInIrish()) {
			setTitle(R.string.title_activity_locate_me_ie);
			((Button) findViewById(R.id.locate_me_button)).setText(R.string.locate_me_label_ie);
			
			webView.loadUrl(EMERGENCY_CONTACTS_URL_IE);
		}
		else {
			webView.loadUrl(EMERGENCY_CONTACTS_URL);
		}
		
		initWebView(webView);
	}
	
	private void initLocationListener() {
		locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		
		Criteria cFine = new Criteria();
		cFine.setAccuracy(Criteria.ACCURACY_FINE);
		String fineProvider = locManager.getBestProvider(cFine, true);
		if(fineProvider != null)
			locManager.requestLocationUpdates(fineProvider, 250, 5, this);
		
		Criteria c = new Criteria();
		c.setAccuracy(Criteria.ACCURACY_COARSE);
		String coarseProvider = locManager.getBestProvider(c, true);
		if(coarseProvider != null)
			locManager.requestLocationUpdates(coarseProvider, 250, 5, this);
		
		Criteria criteria = new Criteria();
		provider = locManager.getBestProvider(criteria, false);
		location = locManager.getLastKnownLocation(provider);
		
		if(location != null)
			onLocationChanged(location);
	}
	
	public void getMyLocation(View view) {
		if(location != null) {
			DialogUtils.showGpsCoordsDialog(this, String.valueOf(roundGpsLocation(location.getLatitude())), String.valueOf(roundGpsLocation(location.getLongitude())));
		}
		else {
			if(KilkennyCastleApplication.getPrefsInstance().viewInIrish())
				Toast.makeText(this, R.string.no_location_toast_ie, Toast.LENGTH_SHORT).show();
			else
				Toast.makeText(this, R.string.no_location_toast, Toast.LENGTH_SHORT).show();
		}
	}
	
	private double roundGpsLocation(double point) {
		DecimalFormat df = new DecimalFormat("##.######");
		return Double.valueOf(df.format(point));
	}
	
	@SuppressLint("SetJavaScriptEnabled")
	private void initWebView(WebView webView) {
		webView.setWebViewClient(new MobaViewClient());
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setNeedInitialFocus(false);
		webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
	}
	
	private class MobaViewClient extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if(url.startsWith("tel:")) {
				startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(url)));
			}
			else if(url.startsWith("http")) {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
			}
			else if(url.startsWith("sms")) {
				Intent smsIntent = new Intent(Intent.ACTION_SENDTO);
				smsIntent.setData(Uri.parse(url));
				startActivity(Intent.createChooser(smsIntent, "Send"));
			}
			else if(url.startsWith("mailto")) {
				MailTo mt = MailTo.parse(url);
				Intent emailIntent = new Intent(Intent.ACTION_SEND);
				emailIntent.setType("text/plain");
				emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{mt.getTo()});
				startActivity(Intent.createChooser(emailIntent, "Send"));//emailIntent);
			}
			else {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
			}
			return true;
		}
	}
	
	@Override
	protected void onPause() {
		locManager.removeUpdates(this);
		super.onPause();
	}
	
	@Override
	protected void onDestroy() {
		locManager.removeUpdates(this);
		super.onDestroy();
	}
	
	@Override
	protected void onResume() {
		initLocationListener();
		super.onResume();
	}

	@Override
	public void onLocationChanged(Location location) {
		Log.v("LOCATION", "pro: " + location.getProvider());
		this.location = location;
	}

	@Override
	public void onProviderDisabled(String provider) {}

	@Override
	public void onProviderEnabled(String provider) {}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {}

}
