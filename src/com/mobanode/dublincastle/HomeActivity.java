package com.mobanode.dublincastle;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.flurry.android.FlurryAgent;
import com.mobanode.castletownhouse.R;
import com.mobanode.prefs.AppPrefs;
import com.mobanode.resourcescommon.imgviewresize.ResizableImageView;
import com.mobanode.utils.AppConstants;
import com.mobanode.utils.DublinCastleActivity;

public class HomeActivity extends DublinCastleActivity {

	private AppPrefs prefs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		prefs = new AppPrefs(this);

		performLanguageChange();
	}

	public void homeBtnsClick(View view) {
		switch (view.getId()) {
		case R.id.tour_image_button:
			FlurryAgent.logEvent("tour_btn_click");
			startActivity(new Intent(this, TourActivity.class));
			break;

		case R.id.info_image_button:
			FlurryAgent.logEvent("info_btn_click");
			startActivity(new Intent(this, InfoActivity.class));
			break;
		case R.id.audio_image_button:
			FlurryAgent.logEvent("audio_btn_click");
			startActivity(new Intent(this, AudioActivity.class));
			break;
		case R.id.home_info_button:
			FlurryAgent.logEvent("home_info_button");
			startActivity(new Intent(this, HomeInfoActivity.class));
			break;

		default:
			break;
		}
	}

	private void performLanguageChange() {
		if (prefs.viewInIrish()) {
			((ResizableImageView) findViewById(R.id.info_image_button))
					.setImageDrawable(getResources().getDrawable(
							R.drawable.info_button_ie));
			((ResizableImageView) findViewById(R.id.tour_image_button))
					.setImageDrawable(getResources().getDrawable(
							R.drawable.tour_button_ie));
		} else {
			((ResizableImageView) findViewById(R.id.info_image_button))
					.setImageDrawable(getResources().getDrawable(
							R.drawable.info_button));
			((ResizableImageView) findViewById(R.id.tour_image_button))
					.setImageDrawable(getResources().getDrawable(
							R.drawable.tour_button));
		}
	}
	
	
	

	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if (prefs.isMusicPlaying() && menu.findItem(MENU_ITEM_ID) == null) {
			menu.add(Menu.NONE, MENU_ITEM_ID, 0, "Play").setIcon(R.drawable.play_button);

			
		}
		else if(!prefs.isMusicPlaying()&& menu.findItem(MENU_ITEM_ID) != null){
			menu.removeItem(MENU_ITEM_ID);
		}
		return true;
	}
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.home, menu);
		if (prefs.viewInIrish()) {
			menu.findItem(R.id.action_change_lang).setTitle(R.string.change_lang_btn_label_en);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_change_lang:
			prefs.setViewInIrish(!prefs.viewInIrish());
			performLanguageChange();
			item.setTitle(prefs.viewInIrish() ? R.string.change_lang_btn_label_en
					: R.string.change_lang_btn_label_ie);
			break;

		case MENU_ITEM_ID:
			if (prefs.isMusicPlaying()) {
				pauseAudio();
				item.setTitle("Play");
				item.setIcon(R.drawable.play_button);
			} else {
				playAudio();	
				item.setTitle("Pause");
				item.setIcon(R.drawable.pause_button);
			}
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onStart() {
		FlurryAgent.onStartSession(this, AppConstants.FLURRY_API_KEY);
		super.onStart();
	}

	@Override
	protected void onStop() {
		FlurryAgent.onEndSession(this);
		super.onStop();
	}

}
