package com.mobanode.dublincastle;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import com.mobanode.castletownhouse.R;

@SuppressLint("SetJavaScriptEnabled")
public class EventActivity extends Activity {
	
	private TextView title;
	private WebView content;
	
	private final String webViewCSS = "<style type=\"text/css\">img {width:100%; height:auto;} body{color: #000000; background:#ffffff;}</style>";
	
	private final String contentSplitter = "<a rel=\"nofollow\"";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event);
		
		title = (TextView) findViewById(R.id.event_header_title);
		content = (WebView) findViewById(R.id.event_content_web_view);
		
		String titleStr = getIntent().getStringExtra("title");
		String contentStr = getIntent().getStringExtra("content");
		
		if(titleStr != null && contentStr != null) {
			setTitle(titleStr);
			
			String[] contentArray = contentStr.split(contentSplitter);
			
			setEventInfo(titleStr, contentArray[0]);
		}
	}
	
	private void setEventInfo(String titleStr, String contentStr) {
		this.title.setText(titleStr);
		this.content.loadDataWithBaseURL(null, webViewCSS + contentStr, "text/html", "UTF-8", null);
		
		initWebView(content);
	}
	
	private void initWebView(WebView webView) {
		webView.setWebViewClient(new MobaViewClient());
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setNeedInitialFocus(false);
		webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
	}
	
	private class MobaViewClient extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if(url.startsWith("tel:")) {
				startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(url)));
			}
			else if(url.startsWith("http")) {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
			}
			else if(url.startsWith("sms")) {
				Intent smsIntent = new Intent(Intent.ACTION_SENDTO);
				smsIntent.setData(Uri.parse(url));
				startActivity(Intent.createChooser(smsIntent, "Send"));
			}
			else if(url.startsWith("mailto")) {
				MailTo mt = MailTo.parse(url);
				Intent emailIntent = new Intent(Intent.ACTION_SEND);
				emailIntent.setType("text/plain");
				emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{mt.getTo()});
				startActivity(Intent.createChooser(emailIntent, "Send"));//emailIntent);
			}
			else {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
			}
			return true;
		}
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.event, menu);
//		return true;
//	}

}
