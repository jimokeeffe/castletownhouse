package com.mobanode.dublincastle.carousel;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mobanode.dublincastle.GalleryActivity;
import com.mobanode.dublincastle.InfoActivity;
import com.mobanode.dublincastle.InfoDetailActivity;
import com.mobanode.dublincastle.IslandEventsActivity;
import com.mobanode.dublincastle.KilkennyCastleApplication;
import com.mobanode.dublincastle.MapsActivity;
import com.mobanode.castletownhouse.R;
import com.mobanode.dublincastle.TourStopActivity;
import com.mobanode.prefs.AppPrefs;
import com.mobanode.resourcescommon.imgviewresize.ResizableImageView;

public class CarouselInfoFragment extends Fragment {
	
	private AppPrefs prefs = KilkennyCastleApplication.getPrefsInstance();
	
	private InfoActivity context;
	
	private final int[] infoImagesEN = {
			R.drawable.info_4_en,
			R.drawable.info_2_en,
			R.drawable.info_3_en,
			R.drawable.info_1_en,
		
			
	};
	
	private final int[] infoImagesIE = {
			R.drawable.info_4_en,
			R.drawable.info_2_en,
			R.drawable.info_3_en,
			R.drawable.info_1_en,
			
			
	};
	
	private final String[] infoImagesTags = {
			"castle_info",
			"map_location",
			"castle_events",
			"gallery"
			
		
			//,
			//"social"
	};
	
	public static Fragment newInstance(InfoActivity context, int pos, float scale) {
		Bundle b = new Bundle();
		b.putInt("pos", pos);
		b.putFloat("scale", scale);
		return Fragment.instantiate(context, CarouselInfoFragment.class.getName(), b);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		
		context = (InfoActivity) this.getActivity();
		
		LinearLayout l = (LinearLayout) inflater.inflate(R.layout.tour_fragment, container, false);
		
		ResizableImageView image = (ResizableImageView) l.findViewById(R.id.tour_car_image_view);
		
		final int pos = this.getArguments().getInt("pos");
		
		if(prefs.viewInIrish())
			image.setImageDrawable(context.getResources().getDrawable(infoImagesIE[pos]));
		else
			image.setImageDrawable(context.getResources().getDrawable(infoImagesEN[pos]));
		
		image.setTag(infoImagesTags[pos]);
		
		image.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String tag = (String) v.getTag();
				if ( tag == null){
					Log.v("tagtest", " tag is null");
				}
				else {
					Log.v("tagtest", " tag is " + tag);
				}
				if(tag.equals("castle_info")) {
					Intent intent = new Intent(context, TourStopActivity.class);
					intent.putExtra("tag", tag);
					startActivity(intent);
				}
				else if(tag.equals("map_location")) {
					Intent mapIntent = new Intent(context, MapsActivity.class);
					mapIntent.putExtra("mapType", "all");
					startActivity(mapIntent);
				}
				else if(tag.equals("castle_events")) {
					Intent eventsIntent = new Intent(context, IslandEventsActivity.class);
					startActivity(eventsIntent);
				}
//				else if(tag.equals("social")) {
//					startActivity(new Intent(context, SocialActivity.class));
//				}
				else if(tag.equals("gallery")) {
					Intent intent = new Intent(context, GalleryActivity.class);
					intent.putExtra("tag", tag);
					startActivity(intent);
				}
				else if(tag.equals("tea_room")) {
					Intent intent = new Intent(context, InfoDetailActivity.class);
					intent.putExtra("tag", tag);
					startActivity(intent);
				}
				else if(tag.equals("butler_gallery")) {
					Intent intent = new Intent(context, InfoDetailActivity.class);
					intent.putExtra("tag", tag);
					startActivity(intent);
					
				}
				else if(tag.equals("castle_parkland")) {
					Intent intent = new Intent(context, InfoDetailActivity.class);
					intent.putExtra("tag", tag);
					startActivity(intent);
				}
//				else if(tag.equals("castle_attractions")) {
//					Intent intent = new Intent(context, AttractionActivity.class);
//					intent.putExtra("tag", tag);
//					startActivity(intent);
//				}
				
			}  
		});
		
		return l;
	}

}
