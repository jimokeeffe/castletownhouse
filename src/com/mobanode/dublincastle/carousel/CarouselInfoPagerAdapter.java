package com.mobanode.dublincastle.carousel;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.widget.TextView;

import com.mobanode.dublincastle.InfoActivity;
import com.mobanode.dublincastle.KilkennyCastleApplication;
import com.mobanode.prefs.AppPrefs;
import com.mobanode.utils.AppConstants;

public class CarouselInfoPagerAdapter extends FragmentStatePagerAdapter implements 
OnPageChangeListener {
	
	private TextView textView ;
	private InfoActivity context;
	private float scale;
	private AppPrefs prefs = KilkennyCastleApplication.getPrefsInstance();
	private final String[] titleText = {"Castle Info", "Map", "Events", "Gallery"};
	private final String[] titleTextie = {"Eolas ar an gCaisle�n", "L�arsc�l", "Imeachta�", "Gaileara�"};
	
	public CarouselInfoPagerAdapter(FragmentManager fm, InfoActivity context, TextView textView) {
		super(fm);
		this.context = context;
		this.textView = textView ;
	}
	
	@Override
	public Fragment getItem(int position) {
		scale = AppConstants.BIG_SCALE;
		return CarouselInfoFragment.newInstance(context, position, scale);
	}
	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//		if (positionOffset >= 0f && positionOffset <= 1f) {
//			cur = getRootView(position);
			
//			if((position + 1) != AppConstants.HOME_PAGES)
//				next = getRootView(position +1);

//			cur.setScaleBoth(AppConstants.BIG_SCALE - AppConstants.DIFF_SCALE * positionOffset);
			
//			if((position + 1) != AppConstants.HOME_PAGES)
//				next.setScaleBoth(AppConstants.SMALL_SCALE + AppConstants.DIFF_SCALE * positionOffset);
//		}
	}
	
	

	@Override
	public int getCount() {
		return AppConstants.INFO_PAGES;
	}
	@Override
	public void onPageScrollStateChanged(int position) { }
	
	
	@Override
	public void onPageSelected(int position) {
//		HomeActivity.carTitleText.setText(titleText[position]);
		if(prefs.viewInIrish()) {
			textView.setText(titleTextie[position]);
		} else {
			textView.setText(titleText[position]);
		}
		Log.v("test", "test completed");
	}


}
