package com.mobanode.dublincastle.carousel;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mobanode.dublincastle.KilkennyCastleApplication;
import com.mobanode.dublincastle.TourActivity;
import com.mobanode.prefs.AppPrefs;
import com.mobanode.utils.AppConstants;

public class CarouselTourPagerAdapter extends FragmentStatePagerAdapter implements
		OnPageChangeListener {
	
//	private CarouselLinearLayout cur = null;
//	private CarouselLinearLayout next = null;
	private TourActivity context;
	private TextView textView;
	private float scale;
	private AppPrefs prefs = KilkennyCastleApplication.getPrefsInstance();
//	private boolean isFristRun = true;
	
	private final String[] titleText = {"The Entrance Hall", "The Staircase Hall", "The Dining Room", "The Butlers Pantry", "The Axial Corridor", "The Brown Study", "The Red Drawing Room","Recption Room", "The Print Room", "The State Bedroom", "The Healy Room","The Map Room","Blue North Bedroom", "Lady Louisa's Boudoir", "Lady Louisa's Bedroom", "Pastel Room", "The Long Gallery", "Conolly's Folly", "The Wonderful Barn", "CastleTown Desmesne"};
	private final String[] titleTextie = {"An Dorchla Isteach", "An Forhalla", "Ceann Staighre an Tua Chatha", "Seomra Sh�amais U� Chonghaile", "Seomra Ghr�naird", "Seomra Leapa an R�", "Seomra na nEala�on agus na nEola�ochta�", "Seomra Leapa na Banr�ona", "An Dorchla St�it", "Seomra Apoll�", "Seomra Caidrimh","Seomra na R�chathaoireach", "Gaileara� na bPicti�r ", "Seomra Wedgewood", "An Seomra Gotach","Halla Ph�draig","An Cl�s M�r","An Fhochroit","An S�ip�al R�oga","Deireadh An Turais"};
	public CarouselTourPagerAdapter(FragmentManager fm, TourActivity context, TextView textView) {
		super(fm);
		this.context = context;
		this.textView = textView;
	}

	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//		if (positionOffset >= 0f && positionOffset <= 1f) {
//			cur = getRootView(position);
			
//			if((position + 1) != AppConstants.HOME_PAGES)
//				next = getRootView(position +1);

//			cur.setScaleBoth(AppConstants.BIG_SCALE - AppConstants.DIFF_SCALE * positionOffset);
			
//			if((position + 1) != AppConstants.HOME_PAGES)
//				next.setScaleBoth(AppConstants.SMALL_SCALE + AppConstants.DIFF_SCALE * positionOffset);
//		}
	}
	
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		Log.v("HOME_PAGER_ADAPTER", "Destroy Item");
		super.destroyItem(container, position, object);
	}

	@Override
	public Fragment getItem(int position) {
//		if (position == 0 && isFristRun) {
//			isFristRun = false;
        	scale = AppConstants.BIG_SCALE;
//		}
//        else
//        	scale = AppConstants.SMALL_SCALE;
		
        return CarouselTourFragment.newInstance(context, position, scale);
	}

	@Override
	public int getCount() {
		return AppConstants.TOUR_PAGES;// * AppConstants.LOOPS;
	}
	
//	private CarouselLinearLayout getRootView(int position)
//	{
//		CarouselHomeFragment frag = (CarouselHomeFragment) this.instantiateItem(HomeActivity.homePager, position);
//		return (CarouselLinearLayout) frag.getView().findViewById(R.id.root);
////		return null;
//	}
	
	@Override
	public void onPageScrollStateChanged(int position) { }
	
	
	@Override
	public void onPageSelected(int position) {
//		HomeActivity.carTitleText.setText(titleText[position]);
		Log.v("test", "test completed");
		
		if(prefs.viewInIrish()) {
			textView.setText(titleTextie[position]);
		} else {
			textView.setText(titleText[position]);
		}
		
	}

}
