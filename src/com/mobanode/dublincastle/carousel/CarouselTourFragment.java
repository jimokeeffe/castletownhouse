package com.mobanode.dublincastle.carousel;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mobanode.dublincastle.KilkennyCastleApplication;
import com.mobanode.castletownhouse.R;
import com.mobanode.dublincastle.TourActivity;
import com.mobanode.dublincastle.TourStopActivity;
import com.mobanode.prefs.AppPrefs;
import com.mobanode.resourcescommon.imgviewresize.ResizableImageView;
import com.mobanode.utils.AppConstants;

public class CarouselTourFragment extends Fragment {
	
	private AppPrefs prefs = KilkennyCastleApplication.getPrefsInstance();

	private TourActivity context;
//	private final int[] homeImages = {R.drawable.menu_festivals, R.drawable.menu_map, R.drawable.menu_my_festivals, R.drawable.menu_social, R.drawable.menu_info};
	
	private final int[] tourImagesEN = {
			R.drawable.entrance, //entrance
			R.drawable.staircase_hall, //staircase hall
			R.drawable.dining_room, //dining room
			R.drawable.butlers_pantry, // butlers pantry
			R.drawable.axial_corridor, // axial corridor
			R.drawable.brown_study, //brown study
			R.drawable.red_silk_room,// red drawing room
			R.drawable.red_silk_room, // recption room
			R.drawable.print_room, //print room
			R.drawable.state_bedroom,	//state bedroom
			R.drawable.healy_room, //healy room
			R.drawable.map_room,// map room
			R.drawable.blue_bedroom,	// blue north bedroom
			R.drawable.lady_boudoir, //lady boudoir
			R.drawable.lady_bedroom, // lady bedroom
			R.drawable.pastel_room, // pastel room
			R.drawable.long_gallery, // long gallery
			R.drawable.conolly_folly, //conolloy folly
			R.drawable.wonderful_barn, // wonderful barn
			R.drawable.desmesne //castletown desmesne
									//batty lodge
	};
	private final int[] tourImagesIE = {
			R.drawable.entrance, //entrance
			R.drawable.staircase_hall, //staircase hall
			R.drawable.dining_room, //dining room
			R.drawable.butlers_pantry, // butlers pantry
			R.drawable.axial_corridor, // axial corridor
			R.drawable.brown_study, //brown study
			R.drawable.red_silk_room,// red drawing room
			R.drawable.red_silk_room, // recption room
			R.drawable.print_room, //print room
			R.drawable.state_bedroom,	//state bedroom
			R.drawable.healy_room, //healy room
			R.drawable.map_room,// map room
			R.drawable.blue_bedroom,	// blue north bedroom
			R.drawable.lady_boudoir, //lady boudoir
			R.drawable.lady_bedroom, // lady bedroom
			R.drawable.pastel_room, // pastel room
			R.drawable.long_gallery, // long gallery
			R.drawable.conolly_folly, //conolloy folly
			R.drawable.wonderful_barn, // wonderful barn
			R.drawable.desmesne //castletown desmesne
									//batty lodge
	};
	
//	private final String[] tourImagesTags = {
//			"pier",
//			"protestant_school", 
//			"muiris_house",
//			"o_suilleabhains_house",
//			"rinn_na_chaisleain",
//			"tomas_house",
//			"post_office",
//			"national_school",
//			"village_well",
//			"an_dail",
//			"the_kings_house",
//			"peigs_house",
//			"the_strand",
//			"the_tower",
//			"the_fort",
//			"bright_dwellings"
//	};
	
	public static Fragment newInstance(TourActivity context, int pos, float scale) {
		Bundle b = new Bundle();
		b.putInt("pos", pos);
		b.putFloat("scale", scale);
		return Fragment.instantiate(context, CarouselTourFragment.class.getName(), b);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		
		context = (TourActivity) this.getActivity();
		
//		ImageView image = new ImageView(context);
//		image.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
//		image.setAdjustViewBounds(true);
//		image.setBackground(context.getResources().getDrawable(R.drawable.car_img_bg));
//		image.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.car_img_bg));
		
		LinearLayout l = (LinearLayout) inflater.inflate(R.layout.tour_fragment, container, false);
		
		ResizableImageView image = (ResizableImageView) l.findViewById(R.id.tour_car_image_view);
		
		final int pos = this.getArguments().getInt("pos");
		
		if(prefs.viewInIrish())
			image.setImageDrawable(context.getResources().getDrawable(tourImagesIE[pos]));
		else
			image.setImageDrawable(context.getResources().getDrawable(tourImagesEN[pos]));
		
		image.setTag(AppConstants.TOUR_IMAGES_TAGS[pos]);
		
//		ImageView carImage = (ImageView) l.findViewById(R.id.carousel_image);
		
//		try {
//			carImage.setImageDrawable(context.getResources().getDrawable(homeImages[pos]));
//		} catch (OutOfMemoryError e) {
//			Log.v("HOME-FRAGMENT", "Out of memory. GC called");
//			System.gc();
//			
//			try {
//				carImage.setImageDrawable(context.getResources().getDrawable(homeImages[pos]));
//			} catch (OutOfMemoryError e2) {
//				Log.v("HOME-FRAGMENT", "Out of memory AGAIN. GC called");
//				System.gc();
//			}
//		}
		
		image.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String tag = (String) v.getTag();
				
				Log.v("TAG", tag);
				
				Intent intent = new Intent(context, TourStopActivity.class);
				intent.putExtra("tag", tag);
				startActivity(intent);
			}
		});
		
//		CarouselLinearLayout root = (CarouselLinearLayout) l.findViewById(R.id.root);
//		float scale = this.getArguments().getFloat("scale");
//		root.setScaleBoth(scale);
		
		return l;
	}
}
