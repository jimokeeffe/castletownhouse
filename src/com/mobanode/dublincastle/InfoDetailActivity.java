package com.mobanode.dublincastle;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import com.mobanode.castletownhouse.R;
import com.mobanode.prefs.AppPrefs;
import com.mobanode.resourcescommon.imgviewresize.ResizableImageView;
import com.mobanode.utils.AppConstants;
import com.mobanode.utils.MobaWebViewClient;

@SuppressLint("SetJavaScriptEnabled") 
public class InfoDetailActivity extends Activity {
	
	private final String IRISH_URL_SUFFIX = "_ie.html";
	private final String ENGLISH_URL_SUFFIX = "_en.html";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_info_detail);
		
		AppPrefs prefs = new AppPrefs(this);
		String tag = getIntent().getStringExtra("tag");
		
		WebView webView = (WebView) findViewById(R.id.info_detail_webview);
		
		if(prefs.viewInIrish()) {
			webView.loadUrl(AppConstants.ASSETS_HTML_FOLDER_PATH + tag + IRISH_URL_SUFFIX);
			
		}
		else {
			webView.loadUrl(AppConstants.ASSETS_HTML_FOLDER_PATH + tag + ENGLISH_URL_SUFFIX);
		}
		
		webView.setWebViewClient(new MobaWebViewClient(this));
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setNeedInitialFocus(false);
		webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
		
		ResizableImageView imageView = (ResizableImageView) findViewById(R.id.dublin_castle_header);
		if(tag == null){
			Log.v("tag test","tag os null" );
		}
		 if(tag.equals("")) {
			 imageView.setImageDrawable(getResources().getDrawable(R.drawable.red_silk_1));
			 setTitle("Parade Tower Facalities");
			
		}
		else if(tag.equals("map_location")) {
			imageView.setImageDrawable(getResources().getDrawable(R.drawable.red_silk_1));
			 setTitle("Castle Tea Room");
		
		}
		else if(tag.equals("castle_events")) {
			imageView.setImageDrawable(getResources().getDrawable(R.drawable.red_silk_1));
			 setTitle("Butler Gallery");
			
		}
		else if(tag.equals("gallery")) {
			imageView.setImageDrawable(getResources().getDrawable(R.drawable.red_silk_1));
			 setTitle("Castle Parkland");
		}

		
		
	}

	

}
