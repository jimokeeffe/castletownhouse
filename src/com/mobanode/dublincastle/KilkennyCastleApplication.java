package com.mobanode.dublincastle;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

import com.mobanode.prefs.AppPrefs;
import com.mobanode.utils.AppConstants;
import com.mobanode.utils.PlayAudio;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.LoaderSettings;
import com.novoda.imageloader.core.cache.LruBitmapCache;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.PushService;

public class KilkennyCastleApplication extends Application {

	private static AppPrefs prefs;
	
	private static ImageManager imageManager;
	
	private static Context appCon;
	
	private Intent service;
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		service = new Intent(this, PlayAudio.class);
		
		LoaderSettings settings = new LoaderSettings.SettingsBuilder()
		.withDisconnectOnEveryCall(true)
		.withCacheManager(new LruBitmapCache(this, 15))
		.withExpirationPeriod(Long.parseLong("86400000"))
		.build(this);
		
		imageManager = new ImageManager(settings);
		
		Parse.initialize(this, AppConstants.PARSE_APPLICATION_ID, AppConstants.PARSE_CLIENT_KEY);
		PushService.subscribe(this, "", HomeActivity.class);
		PushService.setDefaultPushCallback(this, HomeActivity.class);
		ParseInstallation.getCurrentInstallation().saveInBackground();
		
		appCon = this;
		prefs = new AppPrefs(this);
		
		startService(service);
	}
	
	public static final ImageManager getImageManager() {
		return imageManager;
	}
	
	public static AppPrefs getPrefsInstance() {
		if(prefs == null)
			prefs = new AppPrefs(appCon);
		
		return prefs;
		
	}
	@Override
	public void onTerminate() {
		super.onTerminate();
		stopService(service);
	}
}
