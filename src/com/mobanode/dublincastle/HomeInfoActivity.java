package com.mobanode.dublincastle;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.mobanode.castletownhouse.R;
import com.mobanode.prefs.AppPrefs;
import com.mobanode.resourcescommon.imgviewresize.ResizableImageView;
import com.mobanode.utils.AppConstants;

public class HomeInfoActivity extends Activity {

	private final String TOUR_INFO_IE = "tour_intro_ie.html";
	private final String TOUR_INFO_EN = "tour_intro_en.html";
	
	private AppPrefs prefs = KilkennyCastleApplication.getPrefsInstance();
	
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tour_intro);
		
		WebView webView = (WebView) findViewById(R.id.tour_info_webview);
		
		if(prefs.viewInIrish()) {
			setTitle(R.string.title_activity_tour_ie);
			webView.loadUrl(AppConstants.ASSETS_HTML_FOLDER_PATH + TOUR_INFO_IE);
			((ResizableImageView) findViewById(R.id.home_info_button)).setImageDrawable(getResources().getDrawable(R.drawable.tour_button_ie));
		}
		else
			webView.loadUrl(AppConstants.ASSETS_HTML_FOLDER_PATH + TOUR_INFO_EN);
		
		webView.setWebViewClient(new WebViewClient());
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setNeedInitialFocus(false);
		webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
	}

	
}
