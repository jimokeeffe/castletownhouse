package com.mobanode.dublincastle.adapters;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobanode.castletownhouse.R;
import com.mobanode.entities.FacebookPost;
import com.mobanode.utils.DataCache;

public class FacebookListAdapter extends ArrayAdapter<FacebookPost> {

	private ArrayList<FacebookPost> posts = new ArrayList<FacebookPost>();
	private ArrayList<String> imgUrls = new ArrayList<String>();
	private LayoutInflater vi;
	
	public FacebookListAdapter(Context context, int textViewResourceId, ArrayList<FacebookPost> posts, ArrayList<String> imgUrls) {
		super(context, textViewResourceId, posts);
		this.posts = posts;
		this.vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.imgUrls = imgUrls;
		
		if(DataCache.fbImageCache == null)
			DataCache.fbImageCache = new HashMap<String, Bitmap>();
		
		new DownloadThumbs().execute();
	}
	
	@Override
	public int getCount() {
		try {
			return posts.size();
		} catch (Exception e) {
			return 0;
		}
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if(convertView == null) {
			convertView = vi.inflate(R.layout.list_item_facebook, null);
			
			holder = new ViewHolder();
			
			holder.msgText = (TextView) convertView.findViewById(R.id.fbMessageTextView);
			holder.linkName = (TextView) convertView.findViewById(R.id.fbLinkNameTextView);
			holder.linkText = (TextView) convertView.findViewById(R.id.fbLinkTextView);
			holder.postDate = (TextView) convertView.findViewById(R.id.fbDateTextView);
			holder.thumb = (ImageView) convertView.findViewById(R.id.fbThumbImgView);
			
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		// Reset view and inflate new view
//		convertView = null;
//		convertView = vi.inflate(R.layout.list_item_facebook, null);
		
		// Find Facebook post for current position and set layout based on type of post
		FacebookPost post = posts.get(position);
		
		if (post != null) {
//			final TextView msgText = (TextView) convertView.findViewById(R.id.fbMessageTextView);
//			final TextView linkName = (TextView) convertView.findViewById(R.id.fbLinkNameTextView);
//			final TextView linkText = (TextView) convertView.findViewById(R.id.fbLinkTextView);
//			final TextView postDate = (TextView) convertView.findViewById(R.id.fbDateTextView);
//			final ImageView thumb = (ImageView) convertView.findViewById(R.id.fbThumbImgView);

			holder.msgText.setText(Html.fromHtml(post.getMessage()));
			holder.postDate.setText(post.getPostDate());
			
			holder.linkName.setVisibility(View.GONE);
			holder.linkText.setVisibility(View.GONE);
			holder.thumb.setVisibility(View.GONE);
			
			if (post.getPostType().equals("link")) {
//				msgText.setText(Html.fromHtml(post.getMessage()));
//				postDate.setText(post.getPostDate());
				holder.linkName.setVisibility(View.VISIBLE);
				holder.linkText.setVisibility(View.VISIBLE);
				
				holder.linkName.setText(post.getLinkName());
				holder.linkText.setText(post.getLink());
			}
			else if (post.getPostType().equals("photo")) {
//				msgText.setText(Html.fromHtml(post.getMessage()));
//				postDate.setText(post.getPostDate());
				
				if (DataCache.fbImageCache.containsKey(post.getThumb())){
					holder.thumb.setVisibility(View.VISIBLE);
					holder.thumb.setImageBitmap(DataCache.fbImageCache.get(post.getThumb()));
				}
			}
			else if (post.getPostType().equals("status")) {
//				msgText.setText(Html.fromHtml(post.getMessage()));
//				postDate.setText(post.getPostDate());
			}
			else if (post.getPostType().equals("video")) {
//				msgText.setText(Html.fromHtml(post.getMessage()));
//				postDate.setText(post.getPostDate());
				
				holder.linkName.setVisibility(View.VISIBLE);
				
				holder.linkName.setText(post.getLinkName());

				if (DataCache.fbImageCache.containsKey(post.getThumb())){
					holder.thumb.setVisibility(View.VISIBLE);
					holder.thumb.setImageBitmap(DataCache.fbImageCache.get(post.getThumb()));
				}
			}
		
			if(holder.linkName.getText().toString().equals(""))
				holder.linkName.setVisibility(View.GONE);
			if(holder.linkText.getText().toString().equals(""))
				holder.linkText.setVisibility(View.GONE);
			if(post.getMessage().equals(""))
				holder.msgText.setVisibility(View.GONE);
		}
		return convertView;
	}
	
	private void notifyListOfChange() {
		this.notifyDataSetChanged();
	}
	
	private class ViewHolder {
		public TextView msgText;
		public TextView linkName;
		public TextView linkText;
		public TextView postDate;
		public ImageView thumb;
	}
	
	/* Asynchronous task to download and cache all thumbnails */
	public class DownloadThumbs extends AsyncTask<Void, Void, Void> {
		private Bitmap thumbImg = null;

		@Override
		protected Void doInBackground(Void... params) {
			for(String url : imgUrls){
				if(!DataCache.fbImageCache.containsKey(url)) {
					try {
						URL thumbUrl = new URL(url);
						HttpURLConnection conn = (HttpURLConnection) thumbUrl.openConnection();
						InputStream is = conn.getInputStream();
						thumbImg = BitmapFactory.decodeStream(is);
						DataCache.fbImageCache.put(url, thumbImg);
					} catch (Exception e) {
						e.printStackTrace();
					}
					publishProgress();
				}
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			notifyListOfChange();
			super.onProgressUpdate(values);
		}
	}
}
