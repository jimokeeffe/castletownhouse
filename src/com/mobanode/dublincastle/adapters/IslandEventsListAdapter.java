package com.mobanode.dublincastle.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mobanode.castletownhouse.R;
import com.mobanode.entities.IslandEvent;

public class IslandEventsListAdapter extends ArrayAdapter<IslandEvent> {

	private ArrayList<IslandEvent> events;
	private int listItemLayoutResource;
	
	private LayoutInflater inflater;
	
	public IslandEventsListAdapter(Context context, int textViewResourceId, ArrayList<IslandEvent> events) {
		super(context, textViewResourceId, events);
		
		this.events = events;
		this.listItemLayoutResource = textViewResourceId;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if(convertView == null) {
			convertView = inflater.inflate(listItemLayoutResource, null);
			
			holder = new ViewHolder();
			holder.title = (TextView) convertView.findViewById(R.id.event_title_text_view);
			holder.date = (TextView) convertView.findViewById(R.id.event_date_text_view);
			
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.title.setText(events.get(position).getTitle());
		holder.date.setText(splitDate(events.get(position).getDate()));
		
		return convertView;
	}
	
	private String splitDate(String date) {
		String[] dateArray = date.split(" ");
		
		String newDate = "";
		
		if(dateArray.length > 4) {
			for(int i = 0; i < 4; i ++) {
				newDate += dateArray[i] + " ";
			}
		}
		else
			return date;
		
		return newDate;
	}
	
	private class ViewHolder {
		public TextView title;
		public TextView date;
	}

}
