package com.mobanode.dublincastle.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mobanode.castletownhouse.R;
import com.mobanode.entities.Tweet;

public class TwitterListAdapter extends ArrayAdapter<Tweet> {

	private ArrayList<Tweet> tweets;
	private LayoutInflater vi;
	
	public TwitterListAdapter(Context context, int textViewResourceId, ArrayList<Tweet> tweets) {
		super(context, textViewResourceId, tweets);
		vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.tweets = tweets;
	}
	
	@Override
	public int getCount() {
		try {
			return tweets.size();
		} catch (Exception e) {
			return 0;
		}
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if(convertView == null) {
			convertView = vi.inflate(R.layout.list_item_twitter, null);
			
			holder = new ViewHolder();
			
//			holder.tweetLayout = (LinearLayout) convertView.findViewById(R.id.tweetLayout);
			holder.tweetText = (TextView) convertView.findViewById(R.id.tweetTextView);
			holder.tweetDate = (TextView) convertView.findViewById(R.id.tweetCreatedTextView);
			
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.tweetText.setText(Html.fromHtml(tweets.get(position).getTweetText()));
		holder.tweetDate.setText(tweets.get(position).getTweetCreated());
		
		return convertView;
	}
	
	private class ViewHolder {
//		public LinearLayout tweetLayout;
		public TextView tweetText;
		public TextView tweetDate;
	}
}
