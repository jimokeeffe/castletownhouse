package com.mobanode.dublincastle.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import com.mobanode.castletownhouse.R;
import com.mobanode.dublincastle.TourStopActivity;
import com.mobanode.utils.DataCache;

public class MapLocationOverlay extends ItemizedOverlay<OverlayItem> implements OnClickListener {

	private Context mContext;
	private MapView mMapView;
	
	private LayoutInflater mInflater;
	
	private FrameLayout mapPopup;
	private MapView.LayoutParams popupParams;
	
	private ArrayList<OverlayItem> mOverlays = new ArrayList<OverlayItem>();
	
	private boolean showPopupBtn;
	
	private ImageButton mapPopupBtn;
	private TextView mapPopupName;
	private TextView mapPopupDetails;
	private String tag;
	
	public MapLocationOverlay(Drawable marker, Context context, MapView mapView, boolean showPopupBtn) {
		super(boundCenterBottom(marker));
		
		this.mContext = context;
		this.mMapView = mapView;
		mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.showPopupBtn = showPopupBtn;
		
		initAnnotationPopup(marker);
	}

	@Override
	protected OverlayItem createItem(int i) {
		return mOverlays.get(i);
	}

	@Override
	public int size() {
		return mOverlays.size();
	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent(mContext, TourStopActivity.class);
		intent.putExtra("tag", tag);
		mContext.startActivity(intent);
	}
	
	private void initAnnotationPopup(Drawable pinDrawable) {
		popupParams = new MapView.LayoutParams(MapView.LayoutParams.WRAP_CONTENT, MapView.LayoutParams.WRAP_CONTENT, new GeoPoint(0, 0), MapView.LayoutParams.BOTTOM_CENTER);
		mapPopup = (FrameLayout) mInflater.inflate(R.layout.map_popup, null);
		mapPopup.setPadding(0, 0, 0, pinDrawable.getIntrinsicHeight());
		mapPopupName = (TextView) mapPopup.findViewById(R.id.map_popup_name);
		mapPopupDetails = (TextView) mapPopup.findViewById(R.id.map_popup_details);
		mapPopupBtn = (ImageButton) mapPopup.findViewById(R.id.map_arrow_btn);
		mapPopupBtn.setOnClickListener(this);
		
		if(showPopupBtn)
			mapPopupBtn.setVisibility(View.VISIBLE);
		else
			mapPopupBtn.setVisibility(View.GONE);
		
		removePopup();
	}
	
	@Override
	protected boolean onTap(int index) {
		removePopup();

		OverlayItem overlay = mOverlays.get(index);
		tag = overlay.getSnippet();
		showPopup(overlay);
		
		return true;
	}
	
	@Override
	public boolean onTap(GeoPoint p, MapView mapView) {
		removePopup();
		return super.onTap(p, mapView);
	}
	
	public void addOverlay(OverlayItem overlay) {
		mOverlays.add(overlay);
		populate();
	}
	
	private void removePopup() {
		((Activity) this.mContext).runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				mMapView.removeAllViews();
			}
		});
	}
	
	private void showPopup(OverlayItem overlay) {
		GeoPoint geoPoint = overlay.getPoint();
		popupParams.point = geoPoint;
		
		mapPopupName.setText(overlay.getTitle());
		mapPopupDetails.setText(overlay.getSnippet());
		
		mMapView.addView(mapPopup, popupParams);
		mMapView.getController().animateTo(geoPoint);
		
		DataCache.selectedLat = geoPoint.getLatitudeE6() / 1E6;
		DataCache.selectedLng = geoPoint.getLongitudeE6() / 1E6;
	}
	
	public void showSinglePopup(String title) {
		for(OverlayItem oi : mOverlays)
			if(oi.getTitle().equals(title))
				showPopup(oi);
	}

}
