package com.mobanode.dublincastle.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.mobanode.dublincastle.KilkennyCastleApplication;
import com.mobanode.castletownhouse.R;
import com.mobanode.utils.CommonUtils;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.model.ImageTag;
import com.novoda.imageloader.core.model.ImageTagFactory;

public class GridViewGalleryAdapter extends BaseAdapter {
	
	private final int widthInDP = 80;
	private final int paddingInPx = 1;
	
	private int thumbWidth;
    private int thumbPadding;
    
	private Context ctx;
	private ArrayList<String> thumbUrls;
	
	private ImageTagFactory imgTagFactory;
	private ImageManager imageLoader = KilkennyCastleApplication.getImageManager();
	
	public GridViewGalleryAdapter(Context ctx, ArrayList<String> thumbUrls) {
		this.ctx = ctx;
		this.thumbUrls = thumbUrls;
		
		thumbWidth = CommonUtils.getPixelsFromPadding(ctx, widthInDP);
        thumbPadding = CommonUtils.getPixelsFromPadding(ctx, paddingInPx);
		
		this.imgTagFactory = ImageTagFactory.newInstance(ctx, R.drawable.grid_holder);
		this.imgTagFactory.setErrorImageId(R.drawable.grid_holder);
	}

	@Override
	public int getCount() {
		return thumbUrls.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView;
		
		if(convertView == null) {
			imageView = new ImageView(ctx);
			imageView.setBackgroundColor(ctx.getResources().getColor(R.color.green_color));
        	imageView.setLayoutParams(new GridView.LayoutParams(thumbWidth, thumbWidth));
        	imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        	imageView.setPadding(thumbPadding, thumbPadding, thumbPadding, thumbPadding);
		}
		else {
			imageView = (ImageView) convertView;
		}
		
		ImageTag imgTag = imgTagFactory.build(thumbUrls.get(position), ctx);
		imageView.setTag(imgTag);
		imageLoader.getLoader().load(imageView);
		
		return imageView;
	}

}
