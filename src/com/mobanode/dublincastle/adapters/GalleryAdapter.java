package com.mobanode.dublincastle.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.mobanode.resourcescommon.imgviewresize.ResizableImageView;

public class GalleryAdapter extends BaseAdapter {

	private int[] ids;
	private Context context;
	
	public GalleryAdapter(Context context, int[] ids) {
		this.context = context;
		this.ids = ids;
	}
	
	@Override
	public int getCount() {
		return this.ids.length;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ResizableImageView imageView;
		
		if(convertView == null) {
			imageView = new ResizableImageView(this.context);
			imageView.setAdjustViewBounds(true);
		}
		else {
			imageView = (ResizableImageView) convertView;
		}
		
		imageView.setImageDrawable(this.context.getResources().getDrawable(this.ids[position]));
		
		return imageView;
	}

}
