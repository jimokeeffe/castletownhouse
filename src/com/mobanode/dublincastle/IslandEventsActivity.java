package com.mobanode.dublincastle;

import java.util.ArrayList;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import com.mobanode.castletownhouse.R;
import com.mobanode.dublincastle.adapters.IslandEventsListAdapter;
import com.mobanode.dublincastle.parser.FeedParser;
import com.mobanode.entities.IslandEvent;
import com.mobanode.utils.AppConstants;

public class IslandEventsActivity extends Activity {

	private ProgressDialog mProgress;
	
	private ListView eventsListView;
	
	private IslandEventsListAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_island_events);
		
		if(KilkennyCastleApplication.getPrefsInstance().viewInIrish())
			setTitle(R.string.title_activity_island_events_ie);
		
		eventsListView = (ListView) findViewById(R.id.events_list_view);
		eventsListView.setOnItemClickListener(listClickListener);
		
		new DownloadXMLNewsFeed().execute();
	}
	
	private OnItemClickListener listClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			IslandEvent ie = (IslandEvent) parent.getItemAtPosition(position);
			
			if(ie != null) {
				Intent intent = new Intent(IslandEventsActivity.this, EventActivity.class);
				intent.putExtra("title", ie.getTitle());
				intent.putExtra("content", ie.getContent());
				startActivity(intent);
			}
		}
	};
	
	private class DownloadXMLNewsFeed extends AsyncTask<Void, Void, ArrayList<IslandEvent>>
	{
		private FeedParser parser;
		
		@Override
		protected void onPreExecute() {
			mProgress = ProgressDialog.show(IslandEventsActivity.this, "Please wait...", "Downloading Events", true, true);
			parser = new FeedParser();
			super.onPreExecute();
		}
		
		@Override
		protected ArrayList<IslandEvent> doInBackground(Void... params)
		{
			String xml = downloadXML(AppConstants.FEED_URL);
			
			if(xml.equals("error"))
				return null;
			else
				return parser.parseNewsFeedXML(xml);
		}
		
		@Override
		protected void onPostExecute(ArrayList<IslandEvent> result)
		{
			mProgress.dismiss();
			
			if(result == null)
			{
				// Error
			}
			else
			{
				adapter = new IslandEventsListAdapter(IslandEventsActivity.this, R.layout.list_item_event, result);
				eventsListView.setAdapter(adapter);
			}
			
			super.onPostExecute(result);
		}
		
		public String downloadXML(String url)
		{
			try
			{
				HttpClient httpClient = new DefaultHttpClient();
				HttpGet httpGet = new HttpGet(url);
				HttpResponse response = httpClient.execute(httpGet);
				
				return EntityUtils.toString(response.getEntity());
			}
			catch (Exception e)
			{
				e.printStackTrace();
				return "error";
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.island_events, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_refresh_events:
			new DownloadXMLNewsFeed().execute();
			break;

		default:
			break;
		}
		return false;
	}

}
