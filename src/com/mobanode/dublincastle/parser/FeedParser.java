package com.mobanode.dublincastle.parser;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.util.Log;

import com.mobanode.entities.Attraction;
import com.mobanode.entities.IslandEvent;

public class FeedParser {

	public FeedParser() {
		super();
	}
	
	public ArrayList<IslandEvent> parseNewsFeedXML(String xml) {
		ArrayList<IslandEvent> events = new ArrayList<IslandEvent>();
		
		try {
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser xpp = factory.newPullParser();
			
			xpp.setInput(new StringReader(xml));
			int eventType = xpp.getEventType();
			
			boolean isItem = false;
			boolean titleParsed = false;
			
			String title = "";
			String date = "";
			String content = "";
			
			String tag = "";
			
			while(eventType != XmlPullParser.END_DOCUMENT)
			{
				if(eventType == XmlPullParser.START_TAG)
		        {
		        	tag = xpp.getName();
		        	
		        	if(tag.equals("item"))
		        	{
		        		isItem = true;
		        	}
		        }
				else if(eventType == XmlPullParser.END_TAG)
		        {
		        	if(xpp.getName().equals("item"))
		        	{
		        		isItem = false;
		        		titleParsed = false;
//		        		Log.v("EVENTS_PARSE", "title: " + title);
//		        		Log.v("EVENTS_PARSE", "date: " + date);
//		        		Log.v("EVENTS_PARSE", "content: " + content);
		        		
		        		events.add(new IslandEvent(title, date, content));
		        	}
		        	tag = "";
		        }
				else if(eventType == XmlPullParser.TEXT && isItem)
				{
	        		if(tag.equals("title") && !titleParsed)
	        		{
	        			title = xpp.getText();
	        			titleParsed = true;
	        		}
	        		else if(tag.equals("pubDate"))
	        		{
	        			date = xpp.getText();
	        		}
	        		else if(tag.equals("encoded"))
	        		{
	        			content = xpp.getText();
	        		}
		        }
				eventType = xpp.next();
			}
		}
		catch (XmlPullParserException e)
		{
			e.printStackTrace();
			return null;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
		
		return events;
	}
	
	public  ArrayList<Attraction> parseAttractionsFeed(String xml)
	{
		ArrayList<Attraction> results = new ArrayList<Attraction>();
		
		
		try {
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser xpp = factory.newPullParser();
			
			xpp.setInput(new StringReader(xml));
			int eventType = xpp.getEventType();
			
			boolean isItem = false;
			boolean titleParsed = false;
			
			String title = "";
			double lat = 0.0;
			String content = "";
			double lon = 0.0;
			
			String tag = "";
			
			while(eventType != XmlPullParser.END_DOCUMENT)
			{
				if(eventType == XmlPullParser.START_TAG)
		        {
		        	tag = xpp.getName();
		        	
		        	if(tag.equals("item"))
		        	{
		        		isItem = true;
		        	}
		        }
				else if(eventType == XmlPullParser.END_TAG)
		        {
		        	if(xpp.getName().equals("item"))
		        	{
		        		isItem = false;
		        		titleParsed = false;
		        		Log.v("EVENTS_PARSE", "title: " + title);
		        		Log.v("EVENTS_PARSE", "content: " + content);
		        		Log.v("EVENTS_PARSE", "lat: " + lat);
		        		Log.v("EVENTS_PARSE", "lon:" + lon);
		        		
		        		results.add(new Attraction(title, content, lat, lon));
		        	}
		        	tag = "";
		        }
				else if(eventType == XmlPullParser.TEXT && isItem)
				{
	        		if(tag.equals("title") && !titleParsed)
	        		{
	        			title = xpp.getText();
	        			titleParsed = true;
	        		}
	        		else if(tag.equals("lat"))
	        		{
	        			lat = Double.parseDouble(xpp.getText());
	        		}
	        		else if(tag.equals("encoded"))
	        		{
	        			content = xpp.getText();
	        		}
	        		else if(tag.equals("long"))
	        		{
	        			lon = Double.parseDouble(xpp.getText());
	        		}
	        		
		        }
				eventType = xpp.next();
			}
		}
		catch (XmlPullParserException e)
		{
			e.printStackTrace();
			return null;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
		return results ;
	}
}