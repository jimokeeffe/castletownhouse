package com.mobanode.dublincastle.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.ArrayList;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobanode.castletownhouse.R;
import com.mobanode.entities.MapLocation;

public class MapLocationParser {

	private Context context;
	
	public MapLocationParser(Context context) {
		this.context = context;
	}
	
	public MapLocation getStopLocation(String tag) {
		ArrayList<MapLocation> locations = parseMapLocationsJSON(readFileToStringFromRaw());
		
		for(MapLocation ml : locations) {
			if(ml.getTag().equals(tag))
				return ml;
		}
		
		return null;
	}

	public ArrayList<MapLocation> getMapLocations() {
		return parseMapLocationsJSON(readFileToStringFromRaw());
	}
	
	private ArrayList<MapLocation> parseMapLocationsJSON(String json) {
		Type collectionType = new TypeToken<ArrayList<MapLocation>>(){}.getType();
		
		return new Gson().fromJson(json, collectionType);
	}
	
	private String readFileToStringFromRaw()
	{
		InputStream in = null;
		Writer writer = null;
		
		try {
			in = context.getResources().openRawResource(R.raw.blasket_locations);
			writer = new StringWriter();
			char[] buffer = new char[1024];
			
			Reader reader = new BufferedReader(new InputStreamReader(in));
			
			int i;
			while((i = reader.read(buffer)) != -1) {
				writer.write(buffer, 0, i);
			}
		} catch (IOException e) {
			return null;
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}
		
		return writer.toString();
	}
}
