package com.mobanode.dublincastle;



import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import com.mobanode.castletownhouse.R;
import com.mobanode.dublincastle.carousel.CarouselTourPagerAdapter;
import com.mobanode.prefs.AppPrefs;
import com.mobanode.resourcescommon.imgviewresize.ResizableImageView;
import com.mobanode.utils.DublinCastleFragmentActivity;

public class TourActivity extends DublinCastleFragmentActivity {

	public static ViewPager tourPager;
	
	private CarouselTourPagerAdapter adapter;
	
	private FragmentManager fm;
	
	private AppPrefs prefs = KilkennyCastleApplication.getPrefsInstance();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tour);
		
		fm = this.getSupportFragmentManager();
		
		float margin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, -90, getResources().getDisplayMetrics());
		
		tourPager = (ViewPager) findViewById(R.id.tour_viewpager);
		TextView textView = (TextView) findViewById(R.id.tour_title_textview);
		adapter = new CarouselTourPagerAdapter(fm, this, textView);
		tourPager.setOnPageChangeListener(adapter);
		
		tourPager.setAdapter(adapter);
		tourPager.setCurrentItem(0);
		tourPager.setOffscreenPageLimit(3);
//		tourPager.setPageMargin((int) margin);
		
		
		if(prefs.viewInIrish()) {
			((ResizableImageView) findViewById(R.id.dublin_castle_header)).setImageDrawable(getResources().getDrawable(R.drawable.dublin_castle_header));
			setTitle(R.string.title_activity_tour_ie);
			textView.setText("An Dorchla Isteach");
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if(prefs.isMusicPlaying()){
			getMenuInflater().inflate(R.menu.audio_menu, menu);
		}
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_audio:
			if(prefs.isMusicPlaying()){
				pauseAudio();
				item.setTitle("Play");
				item.setIcon(R.drawable.play_white);
			}else{
				playAudio();
				item.setTitle("Pause");
				item.setIcon(R.drawable.pause_white);
			}
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

}
